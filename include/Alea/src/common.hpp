/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file common.hpp
 * @brief Header file for common classes, functions and macros.
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 *
 * Most of the classes, functions and macros defined here, are included
 * from other libraries. Most of them are (if possible) imported into the
 * Alea namespace as not to spill into the global namespaces and make
 * referencing from within Alea easier. The aim here is to import as much
 * as possible to make programming easier in Alea, but not that much as to
 * cause unnecessary slowdown of file that import common.h.
 */

#ifndef ALEA_COMMON_HPP
#define ALEA_COMMON_HPP

#include <cassert>
#include <utility>
#include <string>
#include <iosfwd>
#include <vector>
#include <map>

#include <boost/shared_ptr.hpp>
#include <boost/scoped_ptr.hpp>

#ifdef __GNUC__
#endif // __GNUC__

#ifdef __clang__
#endif // __clang__

/**
 * @brief The global Alea namespace.
 *
 * Contains all names (functions, classes, etc.) meant for general use.
 *
 **/
namespace Alea {

// Import some names into Alea namespace without spoiling the global namespace.
using ::std::string;
using ::std::pair;
using ::std::vector;
using ::std::map;
using ::std::ostream;
using ::boost::shared_ptr;
using ::boost::scoped_ptr;

/**
 * @brief Namespace for internal alea structs, classes and functions.
 *
 * This namespace contains Alea function, structs and classes that are not
 * meant for the public interface, either because they may clutter the public
 * interface or the may change in the fututure without notice or because the
 * may use language features that are not deemed safe for public use.
 **/
namespace internal {
} // namespace internal

} // namespace Alea

#ifdef __FAST_MATH__
#error \
    Compiler option -ffast-math should never be turned on since it is unsafe to use in numerical code.
#endif

#ifdef DEBUG
#define ALEA_DEBUG
#endif // DEBUG

#ifdef ALEA_DEBUG
#define ALEA_ASSERT( X_ ) assert( X_ )
#else
#define ALEA_ASSERT( X_ )
#endif

#endif // ALEA_COMMON_HPP
