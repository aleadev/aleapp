/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file common_stl.hpp
 * @brief Header file for stl support functions
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#ifndef ALEA_COMMON_STL
#define ALEA_COMMON_STL

#include <vector>
#include <ostream>

// TODO: Separate into hpp and cpp file, add include guards, move into Util?, make vector forward

template <typename T>
std::ostream& operator<<( std::ostream& os, const std::vector<T>& vec )
{
    os << "[";
    for ( unsigned int i = 0; i < vec.size(); i++ )
        os << ( i ? ", " : "" ) << vec[i];
    os << "]";
    return os;
}

#endif // ALEA_COMMON_STL
