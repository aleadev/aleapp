/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file specfun.hpp
 * @brief Header file for mathematical special functions
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 *
 * Most of the functions here will be gathered from existing libraries (e.g. gsl, boost, )
 * and fitting them to the style used in Alea.
 */

#ifndef ALEA_MATHUTIL_SPECFUN_HPP
#define ALEA_MATHUTIL_SPECFUN_HPP

namespace Alea {

double factorial( int n );
double binomial( int n, int k );

} // namespace Alea

#endif // ALEA_MATHUTIL_SPECFUN_HPP
