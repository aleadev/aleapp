/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file numint.hpp
 * @brief Header file for numerical integration methods and support classes
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#ifndef ALEA_MATHUTIL_NUMINT_HPP
#define ALEA_MATHUTIL_NUMINT_HPP

#include <cstddef>

namespace Alea {

class UnaryFunc {
public:
    static double do_eval( double x, void* self_ptr );
    virtual double eval( double x ) = 0;
    virtual ~UnaryFunc();
};

class Quadrature1d {

public:
    Quadrature1d();
    ~Quadrature1d();
    int intervals();
    double quad1d( const UnaryFunc& df, double a, double b );

    double get_last_error() const;
    int get_last_status() const;

private:
    struct workspace;

    double m_last_error;
    int m_last_status;
    size_t m_ws_size;
    workspace* m_ws;

    Quadrature1d( const Quadrature1d& );
    void operator=( const Quadrature1d& );
};

} // namespace Alea

#endif // ALEA_MATHUTIL_NUMINT_HPP
