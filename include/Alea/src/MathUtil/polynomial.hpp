/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file polynomial.hpp
 * @brief Header file for the Polynomial class
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#ifndef ALEA_MATHUTIL_POLYNOMIAL_HPP
#define ALEA_MATHUTIL_POLYNOMIAL_HPP

namespace Alea {

typedef double CoeffType;
typedef Array1d CoeffArray;

/**
 * @brief Encapsulates univariate polynomials in monomial form.
 *
 */
class Polynomial {
public:

    // Constructors
    /**
     * @brief Default constructor creates zero polynomial.
     *
     **/
    Polynomial();

    /**
     * @brief Creates polynomial from Eigen coefficient array.
     *
     * @param coeffs The coefficients as column vector in ascending order.
     **/
    explicit Polynomial( const CoeffArray &coeffs );

    /**
     * @brief Creates polynomial from C-style coefficient array.
     *
     * @param coeffs The coefficients as C-style array in ascending order.
     * @param n The number of coefficients (=degree+1).
     **/
    Polynomial( const CoeffType *coeffs, int n );

    // Named constructors
    /**
     * @brief Return the zero polynomial.
     *
     * @return :Polynomial
     **/
    static Polynomial zero();

    /**
     * @brief Return a constant polynomial
     *
     * @param c The constant.
     * @return :Polynomial
     **/
    static Polynomial constant( const CoeffType &c );

    /**
     * @brief Return the polynomial P(x)=x.
     *
     * @return :Polynomial
     **/
    static Polynomial x();

    /**
     * @brief Create polynomial from roots.
     *
     * @param r The roots.
     * @param a_n Coefficient of highest power (default: 1.0)
     * @return :Polynomial
     **/
    static Polynomial from_roots( Array1d r, double a_n = 1.0 );

    ///@{ @name Basic information and access functions
    /**
     * @brief Return the size of the coefficient array of the polynomial.
     *
     * @return int
     */
    int size() const;

    /**
     * @brief Return the degree of the polynomial.
     *
     * @return int
     */
    int degree() const;

    /**
     * @brief Returns a coefficient of the polynomial.
     *
     * @param index Index of the coefficient (can be arbitrary large).
     * @return CoeffType
     */
    CoeffType operator[]( int index ) const;
    ///@}

    // void normalise(); Polynomial normalised() const;

    ///@{ @name Miscallaneous

    /**
     * @brief Evaluate the polynomial at some value x.
     *
     * @param x The value to evaluate P at x.
     * @return double
     **/
    double evaluate( double x ) const;

    /**
     * @brief Returns the derivative of the polynomial.
     *
     * @return :Polynomial
     **/
    Polynomial differentiate() const;

    /**
     * @brief Returns the integrated polynomial.
     *
     * @param constant The constant of integration, defaults to 0.
     * @return :Polynomial
     **/
    Polynomial integrate( double constant = 0 ) const;
    ///@}

    friend bool operator==( const Polynomial& p1, const Polynomial& p2 );
    friend bool operator!=( const Polynomial& p1, const Polynomial& p2 );

    friend Polynomial operator+( const Polynomial &p );
    friend Polynomial operator-( const Polynomial &p );

    friend Polynomial operator+( const Polynomial &p1, const Polynomial &p2 );
    friend Polynomial operator+( const Polynomial &p, const CoeffType &c );
    friend Polynomial operator+( const CoeffType &c, const Polynomial &p );

    friend Polynomial operator-( const Polynomial &p1, const Polynomial &p2 );
    friend Polynomial operator-( const Polynomial &p, const CoeffType &c );
    friend Polynomial operator-( const CoeffType &c, const Polynomial &p );

    friend Polynomial operator*( const Polynomial &p1, const Polynomial &p2 );
    friend Polynomial operator*( const Polynomial &p, const CoeffType &c );
    friend Polynomial operator*( const CoeffType &c, const Polynomial &p );

    friend Polynomial operator/( const Polynomial &p, const CoeffType &c );

    void operator+=( const Polynomial &p2 );
    void operator+=( const CoeffType &c );
    void operator-=( const Polynomial &p2 );
    void operator-=( const CoeffType &c );
    void operator*=( const Polynomial &p2 );
    void operator*=( const CoeffType &c );
    void operator/=( const CoeffType &c );

    friend std::ostream &operator<<( std::ostream &os, const Polynomial &p );
    string to_string() const;

    // TODO [nicetohave]: evaluation with different types
    // TODO [nicetohave]: polynomial division

    struct WithFormat;
    struct IOFormat;

    static IOFormat default_format;

    WithFormat format( const IOFormat &format ) const;
    std::ostream &put_formatted( std::ostream &os,
            const IOFormat &format ) const;

#ifdef ALEA_TESTING_HOOK
    ALEA_TESTING_HOOK
#endif

private:
    CoeffArray m_coeffs;
};

struct Polynomial::IOFormat {
    string m_symbol;
    string m_exp_symbol;
    string m_mult_symbol;
    bool m_compact;
    bool m_highest_first;
    IOFormat()
        : m_symbol( "x" ), m_exp_symbol( "^" ), m_mult_symbol( "" ),
        m_compact( true ), m_highest_first( false ) {}
    IOFormat symbol( string symbol )
    {
        IOFormat format = *this;
        format.m_symbol = symbol;
        return format;
    }
    IOFormat exp_symbol( string exp_symbol )
    {
        IOFormat format = *this;
        format.m_exp_symbol = exp_symbol;
        return format;
    }
    IOFormat mult_symbol( string mult_symbol )
    {
        IOFormat format = *this;
        format.m_mult_symbol = mult_symbol;
        return format;
    }
    IOFormat highest_first( bool highest_first = true )
    {
        IOFormat format = *this;
        format.m_highest_first = highest_first;
        return format;
    }
    IOFormat lowest_first( bool lowest_first = true )
    {
        IOFormat format = *this;
        format.m_highest_first = !lowest_first;
        return format;
    }
    IOFormat compact( bool compact = true )
    {
        IOFormat format = *this;
        format.m_compact = compact;
        return format;
    }
    IOFormat loose( bool loose = true )
    {
        IOFormat format = *this;
        format.m_compact = !loose;
        return format;
    }
};

struct Polynomial::WithFormat {
    IOFormat m_format;
    const Polynomial *m_poly;
    WithFormat( IOFormat format, const Polynomial *poly );
    friend std::ostream &operator<<( std::ostream &os, const WithFormat &p );
};

} // namespace Alea

#endif // ALEA_MATHUTIL_POLYNOMIAL_HPP
