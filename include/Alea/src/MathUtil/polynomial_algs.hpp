/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file polynomial_algs.hpp
 * @brief Header file for general polynomial algorithms
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#ifndef ALEA_MATHUTIL_POLYNOMIAL_ALGS_HPP
#define ALEA_MATHUTIL_POLYNOMIAL_ALGS_HPP

namespace Alea {

ArrayXd poly_eval( const Array3d& rc, const Array1d& xi );

vector<double> poly_eval( const Array3d& rc, const double& xi );

class Polynomial;
vector<Polynomial> poly_eval( const Array3d& rc, const Polynomial& xi );

/**
 * @brief Compute the squared norm of the polynomials.
 *
 * @param rc The recurrence coefficients of the polynomials.
 * @param norm0 Scaling coefficient (i.e. the norm of P_0), Defaults to 1.0.
 * @return Array1d of the norms of the polynomials.
 **/

Array1d poly_square_norm( const Array3d& rc, double norm0 = 1.0 );

/**
 * @brief Convert recurrence coefficients to monic form.
 *
 * @param rc Recurrence coefficients of the initial polynomials.
 * @return Array3d
 **/
Array3d to_monic_rc( const Array3d& rc );

/**
 * @brief Convert recurrence coefficients to normalised form.
 *
 * The resulting recurrence coefficients lead to polynomials that have the
 * same roots, but are normalised with respect to the induced norm.
 *
 * @param rc Recurrence coefficients of the unnormalised polynomials.
 * @return Array3d
 **/
Array3d to_normalised_rc( const Array3d& rc );

/**
 * @brief Compute Gauss rule from recurrence coefficients.
 *
 * @param rc The recurrence coefficients.
 * @return Quad1d
 **/
Quad1d gauss_rule_from_rc( const Array3d& rc );

} // namespace Alea

#endif // ALEA_MATHUTIL_POLYNOMIAL_ALGS_HPP
