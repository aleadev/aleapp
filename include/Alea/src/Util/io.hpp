/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file io.hpp
 * @brief Header file for IO related functions and classes
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 * @bug The class works (somehow), but is not correct (new concept needed)
 */

#ifndef ALEA_UTIL_IO_HPP
#define ALEA_UTIL_IO_HPP

#include <iosfwd>
#include <Alea/Common>

namespace Alea {

void add_stream_extension( std::ostream& os );

struct push_min_double {
    double m_min_double;
    push_min_double( double min_double );
};
std::ostream& operator<<( std::ostream& os, const push_min_double& pmd );

std::ostream& pop_min_double( std::ostream& os );

struct ScopedMinDouble {
    std::ostream& m_os;
    ScopedMinDouble( std::ostream& os, double min_double );
    ~ScopedMinDouble();
};

// class Formatter;
// class MinDoubleFormatter

// ScopedFormatter sf(std::cout, MinDouble(0.01));
// sf.push(MinDouble(0.0001);

} // namespace Alea

#endif // ALEA_UTIL_IO_HPP
