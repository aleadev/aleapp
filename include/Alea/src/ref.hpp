/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2015 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file ref.hpp
 * @brief Header file for the Alea::Ref class
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#ifndef ALEA__REF_HPP
#define ALEA__REF_HPP

#include <stdexcept>
#include <boost/shared_ptr.hpp>
#include <boost/concept_check.hpp>

namespace Alea {

using boost::shared_ptr;

namespace internal {

/**
 * @brief Function used for shared_ptr's that should never delete their object.
 **/
inline void null_deleter( void const* ) {}

const char* get_ref_error_stored_static_ref();
} // namespace internal

/**
 * @brief Template class to keep references to objects.
 **/
template <class Pointee>
class Ref {
    shared_ptr<Pointee> m_ref;
    bool m_last_owner;
public:
    Ref() : m_ref(), m_last_owner( false ) {}

    template <class Derived>
    explicit Ref( Derived* p )
        : m_ref( p ), m_last_owner( false ) {}

    template <class Derived, class Deleter> Ref( Derived* p,
            Deleter d, bool last_owner = false )
        : m_ref( p, d ), m_last_owner( last_owner ) {}

    template <class Derived> Ref( shared_ptr<Derived> p )
        : m_ref( p ), m_last_owner( false ) {}

    template <class Derived> Ref( const Ref<Derived>& r )
        : m_ref( r.get_shared_ptr() ), m_last_owner( false ) {}

    const shared_ptr<Pointee>& get_shared_ptr() const
    {
        return m_ref;
    }

    template <class Derived>
    Ref& operator=( const Ref<Derived>& other )
    {
        if( this != &other ) {
            m_ref = other.get_shared_ptr();
            m_last_owner = false;
        }
        return *this;
    }
    Ref& operator=( const Ref& other )
    {
        if( this != &other ) {
            m_ref = other.get_shared_ptr();
            m_last_owner = false;
        }
        return *this;
    }

    Pointee* operator->()
    {
        return m_ref.operator->();
    }

    ~Ref()
    {
        if( m_last_owner ) {
            if( !this->m_ref.unique() ) {
                throw std::runtime_error(
                        internal::get_ref_error_stored_static_ref() );
            }
        }
    }

};

template <class Pointee>
inline Ref<Pointee> make_ref( Pointee* p )
{
    return Ref<Pointee>( p );
}

template <class Pointee>
inline Ref<Pointee> make_ref( Pointee& p )
{
    return Ref<Pointee>( &p, internal::null_deleter, true );
}

} // namespace Alea

#endif // ALEA__REF_HPP
