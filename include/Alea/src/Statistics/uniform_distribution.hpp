/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2015 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file uniform_distribution.hpp
 * @brief Header file for the UniformDistribution class
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#ifndef ALEA_STATISTICS_UNIFORM_DISTRIBUTION_HPP
#define ALEA_STATISTICS_UNIFORM_DISTRIBUTION_HPP

#include "distribution.hpp"

namespace Alea {

class UniformDistribution : public ContinuousDistribution {
public:
    UniformDistribution( double a, double b );

    virtual double mean() const;
    virtual double var() const;
    virtual double skewness() const;
    virtual double kurtosis_excess() const;

    virtual double pdf( double x ) const;
    virtual double cdf( double x ) const;
    virtual double invcdf( double y ) const;
    using ContinuousDistribution::pdf;
    using ContinuousDistribution::cdf;
    using ContinuousDistribution::invcdf;
private:
    double m_a;
    double m_b;
};

} // namespace Alea

#endif // ALEA_STATISTICS_UNIFORM_DISTRIBUTION_HPP
