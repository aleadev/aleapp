/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2015 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file translated_distribution.hpp
 * @brief Header file for the TranslatedDistribution class
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#ifndef ALEA_STATISTICS_TRANSLATED_DISTRIBUTION_HPP
#define ALEA_STATISTICS_TRANSLATED_DISTRIBUTION_HPP

#include "distribution.hpp"

namespace Alea {

class TranslatedDistribution : public ContinuousDistribution {
public:
    virtual double pdf( double x ) const;
    virtual double cdf( double x ) const;
    virtual double invcdf( double y ) const;
    virtual ArrayXd pdf( const ArrayXd& x ) const;
    virtual ArrayXd cdf( const ArrayXd& x ) const;
    virtual ArrayXd invcdf( const ArrayXd& y ) const;
private:
    Ref<ContinuousDistribution> m_refdist;
    double m_shift;
    double m_scale;
    double m_center;
};

} // namespace Alea

#endif // ALEA_STATISTICS_TRANSLATED_DISTRIBUTION_HPP
