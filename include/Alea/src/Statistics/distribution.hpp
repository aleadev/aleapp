/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2015 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file distribution.hpp
 * @brief Header file for the Distribution class
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#ifndef ALEA_STATISTICS_DISTRIBUTION_HPP
#define ALEA_STATISTICS_DISTRIBUTION_HPP
#include <Alea/src/types.hpp>

namespace Alea {

class Distribution {
public:
    virtual ~Distribution() {};

    virtual double mean() const = 0;
    virtual double var() const = 0;
    virtual double skewness() const = 0;
    virtual double kurtosis_excess() const = 0;

    //virtual Array1d sample( int n ) const = 0;
    //virtual Ref<Distribution> translate(double shift, double scale);
    //virtual Ref<Distribution> fix_moments(double mean,double var);
    //virtual Ref<Distribution> fix_bounds(double min, double max, double min_prob=1.0);
    //virtual Ref<Distribution> fix_quantiles(double c0, double x0, double c1, double x1);
};

class ContinuousDistribution : public Distribution {
public:
    virtual double pdf( double x ) const = 0;
    virtual double cdf( double x ) const = 0;
    virtual double invcdf( double y ) const = 0;

    virtual ArrayXd pdf( const ArrayXd& x ) const;
    virtual ArrayXd cdf( const ArrayXd& x ) const;
    virtual ArrayXd invcdf( const ArrayXd& y ) const;

    virtual Array1d sample( int n ) const;

    //virtual Ref<ContinuousDistribution> translate(double shift, double scale);
    //virtual Ref<ContinuousDistribution> fix_moments(double mean,double var);
    //virtual Ref<ContinuousDistribution> fix_bounds(double min, double max);
};

class DiscreteDistribution : public Distribution {
public:
    virtual double pmf( int x ) const = 0;
    virtual double cdf( int x ) const = 0;
    virtual int invcdf( double y ) const = 0;

    virtual ArrayXd pmf( const ArrayXi& x ) const;
    virtual ArrayXd cdf( const ArrayXi& x ) const;
    virtual ArrayXi invcdf( const ArrayXd& y ) const;

    virtual Array1i sample( int n ) const;
};

} // namespace Alea

#endif // ALEA_STATISTICS_DISTRIBUTION_HPP
