/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file eigen_mem_debug.hpp
 * @brief Macros and functions that aid in memory debugging with Eigen
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#ifndef ALEA_EIGEN_MEM_DEBUG_HPP
#define ALEA_EIGEN_MEM_DEBUG_HPP

#ifdef EIGEN_RUNTIME_NO_MALLOC
namespace Alea {

namespace internal {

/**
 * \brief Class to temporarily disallow mem allocation by Eigen.
 */
class EigenDisallowMalloc {
public:
    EigenDisallowMalloc( bool allow = false )
        : old_state( Eigen::internal::is_malloc_allowed() )
    {set_allow( allow ); }
    ~EigenDisallowMalloc() {set_allow( old_state ); }
    void set_allow( bool allow ) {Eigen::internal::set_is_malloc_allowed( allow ); }
private:
    bool old_state;
};

} // namespace internal

} // namespace Alea

#define ALEA_DISALLOW_EIGEN_MALLOC Alea::internal::EigenDisallowMalloc \
    alea_eigen_malloc_allow_guard
#endif

#endif // ALEA_EIGEN_MEM_DEBUG_HPP
