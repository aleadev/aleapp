/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file eigen_throw.hpp
 * @brief Make Eigen throw exceptions instead of aborting on errors
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#ifndef ALEA_EIGEN_THROW_HPP
#define ALEA_EIGEN_THROW_HPP

#ifndef NDEBUG

namespace Alea {
namespace internal {
void eigen_assert_fail( const char *condition, const char *function,
            const char *file, int line );
} // namespace internal
} // namespace Alea

//#define ALEA_ASSERT_INTERNAL()

/*
 * Define eigen_assert before (!) Eigen does it itself. That way we can throw
 * an exception, which is much better than just aborting, since a) we can catch
 * it, b) behaves much nicer in unit tests, as they can continue c) we may even
 * get a stack trace.
 */
#ifdef eigen_assert
#error eigen_assert already defined. This header file must be included before \
    any Eigen header to take effect.
#endif

#define eigen_assert( x ) \
    do { \
        if( !Eigen::internal::copy_bool( x ) ) \
            Alea::internal::eigen_assert_fail( \
                EIGEN_MAKESTRING( x ), \
                __PRETTY_FUNCTION__, \
                __FILE__, __LINE__ ); \
    } while( false )

/*
 * In the debug version we want Eigen matrices to be initialised to be all
 * NaNs. That makes it much easier to recognise errors due to missing
 * initialisation. Note: Initialising to zero only in the debug version
 * would be a catastrophy. Initialising to zero in all versions may incur a
 * performance penalty. Having the default to not initialise in all version is
 * like gambling.
 */
#define EIGEN_INITIALIZE_MATRICES_BY_NAN

#endif // !NDEBUG

#endif // ALEA_EIGEN_THROW_HPP
