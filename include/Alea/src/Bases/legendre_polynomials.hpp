/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file legendre_polynomials.hpp
 * @brief Header file for LegendrePolynomials class
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#ifndef ALEA_BASES_LEGENDRE_POLYNOMIALS_HPP
#define ALEA_BASES_LEGENDRE_POLYNOMIALS_HPP

#include "polynomial_system.hpp"

namespace Alea {

class LegendrePolynomials : public PolynomialSystem {
public:
    virtual Array3d recur_coeffs( int p ) const;

    virtual bool is_monic() const;
    virtual bool is_normalised() const;
    virtual bool is_orthogonal() const;
};

} // namespace Alea

#endif // ALEA_BASES_LEGENDRE_POLYNOMIALS_HPP
