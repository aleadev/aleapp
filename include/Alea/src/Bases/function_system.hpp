/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file function_system.hpp
 * @brief Header file for the FunctionSystem class
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#ifndef ALEA_BASES_FUNCTION_SYSTEM_HPP
#define ALEA_BASES_FUNCTION_SYSTEM_HPP

namespace Alea {

class FunctionSystem {
public:
    virtual ~FunctionSystem() {};
    virtual Array1d norm( int p ) const = 0;
    virtual ArrayXd evaluate( int n, const Array1d& xi ) const = 0;
};

} // namespace Alea

#endif // ALEA_BASES_FUNCTION_SYSTEM_HPP
