/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file polynomial_system.hpp
 * @brief Header file for abstract PolynomialSystem class
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#ifndef ALEA_BASES_POLYNOMIAL_SYSTEM_HPP
#define ALEA_BASES_POLYNOMIAL_SYSTEM_HPP

#include "function_system.hpp"

namespace Alea {

/**
 * @brief Base class for systems of polynomials satisfying a three-term
 * recurrence.
 *
 * Note, that all orthogonal polynomials (like the Hermite, Legendre,
 * Laguerre etc.) satisfy a three-term recurrence. But also the monomials
 * fall into this class. Other systems of polynomials are of less interest
 * in Alea and thus not represented here.
 *
 * Note, that by Favard's theorem (under certain conditions on the
 * recurrence coefficients) there is a unique linear functional L, such
 * that @f$L(P_0)=1@f$ and @f$L(P_i P_j)=0@f$ if @f$i \not= j@f$. This
 * linear functional can be used to compute the norm and structure
 * coefficients of the polynomials from the recurrence coefficients alone.
 *
 * @see http://en.wikipedia.org/wiki/Favard%27s_theorem
 **/
class PolynomialSystem : public FunctionSystem {
public:
    /**
     * @brief Return the recurrence coefficients of the polynomials system.
     *
     * With the returned coefficients polynomials of the given system of maximum
     * degree max_degree can be generated via the recurrence
     *
     * \f[ P_{n+1}(x)=(a_n+b_n x)P_n(x)-c_n P_{n-1}(x)\f]
     *
     * The coefficients @f$a_n@f$, @f$b_n@f$, and @f$c_n@f$ are returned in the
     * columns of returned Array3d.
     *
     * @param max_degree The maximum degree polynomial that can be generated.
     * @return Array3d The coefficients as a max_degree x n array.
     */
    virtual Array3d recur_coeffs( int max_degree ) const = 0;

    /**
     * @brief Return the norms of the polynomials of the polynomials system.
     *
     * @param max_degree The maximum degree to return the norms for.
     * @return Array1d of size max_degree+1 containing the norms for polynomials
     *         of degree 0 to max_degree.
     */
    virtual Array1d norm( int max_degree ) const;

    /**
     * @brief Evaluate the polynomial system
     *
     * Evaluates the polynomial system for all polynomials up to degree
     * max_degree and for all points given in the array xi.
     *
     * @param max_degree The maximum degree of the polynomials to evaluate.
     * @param xi An 1 x m array of points to evaluate the polynials at.
     * @return ArrayXd The (max_degree+1) x m array point evaluations.
     */
    virtual ArrayXd evaluate( int max_degree, const Array1d& xi ) const;

    /**
     * @brief True, if the highest coefficient is always 1.
     *
     * @return bool
     **/
    virtual bool is_monic() const = 0;

    /**
     * @brief True, if the polynomials are orthogonal.
     *
     * Whether the polynomials are orthogonal depends, of course, on the
     * measure under consideration. However, if the polynomials are orthogonal
     * with respect to any measure, this measure is unique and can be
     * inferred from the recurrence coefficients of the polynomials. If such a
     * measure does exist, this method shall return true. (Note: see Favard's
     * theorem).
     *
     * @return bool
     **/
    virtual bool is_orthogonal() const = 0;

    /**
     * @brief True, if the polynomials are normalised.
     *
     * Whether the polynomials are normalised depends, of course, on the
     * measure under consideration. However, if the polynomials are orthogonal
     * with respect to any measure, this measure is unique and the norm can be
     * easily reproduced except for a constant factor that can be chosen such
     * that the norm of @f$P_0@f$ is 1 (see Alea::poly::square_norm). This
     * method shall return true, if the polynomials can be normalised with
     * respect to this induced measure.
     *
     * @return bool
     **/
    virtual bool is_normalised() const = 0;

};

Ref<PolynomialSystem> to_monic( Ref<const PolynomialSystem> poly );
Ref<PolynomialSystem> normalise( Ref<const PolynomialSystem> poly );

} // namespace Alea

#endif // ALEA_BASES_POLYNOMIAL_SYSTEM_HPP
