/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file types.hpp
 * @brief Common types used in Alea
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#ifndef ALEA_TYPES_HPP
#define ALEA_TYPES_HPP

namespace Alea {

typedef Eigen::VectorXd Vector;
typedef Eigen::RowVectorXd RowVector;
typedef Eigen::MatrixXd Matrix;

typedef Eigen::ArrayXXd ArrayXd;
typedef Eigen::Array<double, Eigen::Dynamic, 1> Array1d;
typedef Eigen::Array<double, Eigen::Dynamic, 3> Array3d;

typedef Eigen::ArrayXXi ArrayXi;
typedef Eigen::Array<int, Eigen::Dynamic, 1> Array1i;
typedef Eigen::Array<int, Eigen::Dynamic, 3> Array3i;

typedef Eigen::Ref<const Array1d> Array1dCRef;

struct Quad1d {
    Array1d x;
    Array1d w;
};

} // namespace Alea

#endif // ALEA_TYPES_HPP
