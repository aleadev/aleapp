# The following options work for gcc and mostly for clang. Options for other
# compilers in the same spirit as the ones here, should be added when known.

# Turn on all warning options (all+pedantic+extra)
add_cxx_compile_option(-Wall ALL_WARNINGS)
add_cxx_compile_option(-Wpedantic PEDANTIC)
add_cxx_compile_option(-Wextra EXTRA_WARNINGS)

# Warnings about not keeping to the rules in S. Meyers Effective C++ is IMHO
# broken and produces too many spurious messages that can't even be turned of or
# circumvented.
# add_cxx_compile_option(-Weffc++ EFFCPP)

# Warn about conversion errors. This is a good measure against int/double
# conversion errors. With the supression of sign-conversions the amount of
# warnings becomes manageable (note that e.g. std::vector uses `unsigned long`
# and Eigen uses `long` for indexing)
add_cxx_compile_option(-Wconversion WARN_CONVERSION)
add_cxx_compile_option(-Wno-sign-conversion NO_SIGN_CONVERSION)

# We want to keep to the c++03 standard throughout the code, but it would be
# nice if the code was already c++11 compatible.
add_cxx_compile_option(-Wc++11-compat CXX11_COMPAT)
add_cxx_compile_option(-std=c++03 CXX03)

# Certain warnings should really be errors (e.g. if a non-void function exits
# without returning anything, this is just "undefined behaviour" in the C++ 
# standard (any version) and the compiler vendor can do anything at this point)
add_cxx_compile_option(-Werror=return-type NO_RETURN_TYPE_IS_ERROR)

# Makes gcc stop on the first error. This is extremely helpful as otherwise it's
# often difficult to spot the real error that initally caused compilation to
# fail. The default behaviour  of gcc (to continues processing after errors) is
# especially annoying after missing include files or explicit #error directives.
add_cxx_compile_option(-Wfatal-errors ERRORS_ARE_FATAL)

# CLANG options that should be considered
add_cxx_compile_option(-Wabsolute-value ABS_TYPE_CORRECTNESS)
add_cxx_compile_option(-fdiagnostics-show-template-tree SHOW_TEMPLATE_TREE)
add_cxx_compile_option(-fdiagnostics-show-category=id SHOW_CATEGORY_ID)
add_cxx_compile_option(-Qunused-arguments NO_WARN_UNUSED_COMPILE_OPTIONS)

# CLANG can also has certain sanitizers that introduce run-time checks into
# the code, making it easier to find certain kinds of bugs. Those sanitizers
# should be turned on on a case-by-case basis.
#  * AddressSanitizer is a fast memory error detector
#  * MemorySanitizer is a detector of uninitialized reads (slowdown ~ 3x)

#add_cxx_compile_option(-fsanitize=address SANITIZE_ADRESSES)
#add_cxx_compile_option(-fno-omit-frame-pointer NO_OMIT_FRAME_POINTER)

#add_cxx_compile_option(-fsanitize=memory SANITIZE_MEMORY)
#add_cxx_compile_option(-fsanitize-memory-track-origins=2 TRACK_MEMORY_ORIGIN)

add_cxx_compile_option(-fsanitize=undefined SANITIZE_UNDEFINED_BEHAVIOUR)
#add_cxx_compile_option(-fsanitize-undefined-trap-on-error SANITIZE_UNDEFINED_TRAP_ON_ERROR)
#add_cxx_compile_option(-fno-sanitize-recover=null NO_RECOVER)
