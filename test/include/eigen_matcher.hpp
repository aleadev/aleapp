/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file eigen_match.hpp
 * @brief Provides Matchers for Eigen class that can used with Catch
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 * @todo Explain here what matchers are
 */

#ifndef TEST_EIGEN_MATCHER_HPP
#define TEST_EIGEN_MATCHER_HPP

#include "internal/catch_matchers.hpp"
#include <Alea/src/types.hpp>
#include <sstream>

// TODO: put some stuff (and includes) into .cpp file
// TODO: add additional matrix matchers
// TODO: adjust the namespaces used here

using Catch::Matchers::Impl::MatcherImpl;

namespace Alea {

  template<typename Matcher, typename MatrixType>
  struct MatrixMatcherBase : MatcherImpl<Matcher, MatrixType> {
    MatrixMatcherBase( MatrixType const& mat, const std::string& comp_str ) : 
      m_mat( mat ), m_comp_str(comp_str) {}
    MatrixMatcherBase( MatrixMatcherBase const& other ) : m_mat( other.m_mat ){}
    bool check_size( MatrixType const& expr ) const { 
      return (m_mat.rows() == expr.rows()) &&
	      (m_mat.cols() == expr.cols());
    }
    virtual std::string toString() const {
      std::stringstream buf;
      buf << std::endl << m_comp_str << ": "
	  << std::endl << m_mat << std::endl;
      return buf.str();
    }

    MatrixType m_mat;
    std::string m_comp_str;
  };
      
  template<typename MatrixType>
  struct MatrixEquals : MatrixMatcherBase<MatrixEquals<MatrixType>, MatrixType> {
    typedef MatrixMatcherBase<MatrixEquals<MatrixType>, MatrixType> super;
    using super::m_mat;
    MatrixEquals( MatrixType const& mat ) : super( mat, "equals" ){}
    
    virtual bool match( MatrixType const& expr ) const {
      return this->check_size(expr) && (m_mat == expr).all();
    }
  };

  const double def_reltol = 1e-10;
  const double def_abstol = 1e-10;

  template<typename MatrixType>
  struct MatrixClose : MatrixMatcherBase<MatrixEquals<MatrixType>, MatrixType> {
    typedef MatrixMatcherBase<MatrixEquals<MatrixType>, MatrixType> super;
    using super::m_mat;
    MatrixClose( MatrixType const& mat, 
		double reltol=def_reltol, 
		double abstol=def_abstol ) : 
		super( mat, "close to" ), m_abstol(abstol), m_reltol(reltol) {}
    
    virtual bool match( MatrixType const& expr ) const {
      return this->check_size(expr) && 
	    ((m_mat - expr).array().abs() <= m_reltol * expr.array().abs() + m_abstol
	      || m_mat == expr).all();
    }
    double m_reltol;
    double m_abstol;
  };
}

namespace Catch{
  namespace Matchers {
    using namespace Alea;

    inline MatrixEquals<ArrayXd> Equals( const ArrayXd& mat ) {
	return MatrixEquals<ArrayXd>( mat );
    }

    inline MatrixEquals<Array1d> Equals( const Array1d& mat ) {
	return MatrixEquals<Array1d>( mat );
    }

    inline MatrixEquals<Array3d> Equals( const Array3d& mat ) {
	return MatrixEquals<Array3d>( mat );
    }

    inline MatrixEquals<Array1d> Equals( const Vector& mat ) {
	return MatrixEquals<Array1d>( mat );
    }

    inline MatrixEquals<ArrayXd> Equals( const Matrix& mat ) {
	return MatrixEquals<ArrayXd>( mat );
    }

    inline MatrixEquals<ArrayXi> Equals( const ArrayXi& mat ) {
	return MatrixEquals<ArrayXi>( mat );
    }

    inline MatrixEquals<Array1i> Equals( const Array1i& mat ) {
	return MatrixEquals<Array1i>( mat );
    }

    inline MatrixEquals<Array3i> Equals( const Array3i& mat ) {
	return MatrixEquals<Array3i>( mat );
    }

    inline MatrixClose<ArrayXd> CloseTo( const Matrix& mat,
				       double reltol=def_reltol,
				       double abstol=def_abstol) {
	return MatrixClose<ArrayXd>( mat, reltol, abstol );
    }

    inline MatrixClose<ArrayXd> CloseTo( const ArrayXd& mat,
    				       double reltol=def_reltol,
				       double abstol=def_abstol) {
	return MatrixClose<ArrayXd>( mat, reltol, abstol );
    }

    inline MatrixClose<Array1d> CloseTo( const Array1d& mat,
    				       double reltol=def_reltol,
				       double abstol=def_abstol) {
	return MatrixClose<Array1d>( mat, reltol, abstol );
    }

    inline MatrixClose<Array3d> CloseTo( const Array3d& mat,
    				       double reltol=def_reltol,
				       double abstol=def_abstol) {
	return MatrixClose<Array3d>( mat, reltol, abstol );
    }

    inline MatrixClose<Array1d> CloseTo( const Vector& mat,
				       double reltol=def_reltol,
				       double abstol=def_abstol) {
	return MatrixClose<Array1d>( mat, reltol, abstol );
    }

  } // namespace Matchers

} // namespace Catch


#endif // TEST_EIGEN_MATCHER_HPP
