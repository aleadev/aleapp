/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file alea_unittest.hpp
 * @brief Provides the Catch unit test framework and own extensions/modification
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#ifndef TEST_ALEA_UNITTEST_HPP
#define TEST_ALEA_UNITTEST_HPP

#include "catch.hpp"
#include <Alea/Types>
#include "eigen_matcher.hpp"

#ifdef __TESTNAME__
#define ALEA_TEST_CASE(a, b) TEST_CASE(__TESTNAME__ "/" a, b)
#else
#define ALEA_TEST_CASE(a, b) TEST_CASE(a, b)
#endif

#endif // TEST_ALEA_UNITTEST_HPP
