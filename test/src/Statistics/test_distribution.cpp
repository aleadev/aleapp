/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2015 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file test_distribution.cpp
 * @brief Unit tests for Statistics/distribution.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include "alea_unittest.hpp"

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/Statistics/distribution.hpp"

using namespace Alea;

#define BASE_TAGS "[statistics][distribution]"

// Create mock derived class from distribution for testing
class MockDistribution : public ContinuousDistribution {
public:
    virtual double mean() const { return 0; }
    virtual double var() const { return 0; }
    virtual double skewness() const { return 0; }
    virtual double kurtosis_excess() const { return 0; }

    virtual double pdf( double x ) const { return 2 * x + 1; }
    virtual double cdf( double x ) const { return 3 * x + 2; }
    virtual double invcdf( double y ) const { return ( y - 2 ) / 3; }
    using ContinuousDistribution::pdf;
    using ContinuousDistribution::cdf;
    using ContinuousDistribution::invcdf;
};

ALEA_TEST_CASE( "Return value of Distribution.pdf has same shape",
    BASE_TAGS "[pdf]" )
{
    MockDistribution dist;
    {
        Array1d x = Array1d::Random( 7 ), expect( 7 );
        expect = 2 * x + 1;
        REQUIRE_THAT( dist.pdf( x ), CloseTo( expect ) );
    }
    {
        ArrayXd x = ArrayXd::Random( 3, 2 ), expect( 3, 2 );
        expect = 2 * x + 1;
        REQUIRE_THAT( dist.pdf( x ), CloseTo( expect ) );
    }
    {
        Matrix x = Matrix::Random( 4, 5 ), expect( 4, 5 );
        expect = 2 * x.array() + 1;
        REQUIRE_THAT( dist.pdf( x ), CloseTo( expect ) );
    }
}

ALEA_TEST_CASE( "Return value of Distribution.cdf has same shape",
    BASE_TAGS "[cdf]" )
{
    MockDistribution dist;
    {
        Array1d x = Array1d::Random( 7 ), expect( 7 );
        expect = 3 * x + 2;
        REQUIRE_THAT( dist.cdf( x ), CloseTo( expect ) );
    }
    {
        ArrayXd x = ArrayXd::Random( 3, 2 ), expect( 3, 2 );
        expect = 3 * x + 2;
        REQUIRE_THAT( dist.cdf( x ), CloseTo( expect ) );
    }
    {
        Matrix x = Matrix::Random( 4, 5 ), expect( 4, 5 );
        expect = 3 * x.array() + 2;
        REQUIRE_THAT( dist.cdf( x ), CloseTo( expect ) );
    }
}

ALEA_TEST_CASE( "Return value of Distribution.invcdf has same shape",
    BASE_TAGS "[invcdf]" )
{
    MockDistribution dist;
    {
        Array1d x = Array1d::Random( 7 ), expect( 7 );
        expect = ( x - 2 ) / 3;
        REQUIRE_THAT( dist.invcdf( x ), CloseTo( expect ) );
    }
    {
        ArrayXd x = ArrayXd::Random( 3, 2 ), expect( 3, 2 );
        expect = ( x - 2 ) / 3;
        REQUIRE_THAT( dist.invcdf( x ), CloseTo( expect ) );
    }
    {
        Matrix x = Matrix::Random( 4, 5 ), expect( 4, 5 );
        expect = ( x.array() - 2 ) / 3;
        REQUIRE_THAT( dist.invcdf( x ), CloseTo( expect ) );
    }
}
