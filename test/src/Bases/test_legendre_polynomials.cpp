/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file test_legendre_polynomials.cpp
 * @brief Unit tests for Bases/legendre_polynomials.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include "alea_unittest.hpp"

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/Bases/legendre_polynomials.hpp"
#include "polynomial_test_utils.hpp"

using namespace Alea;

#define BASE_TAGS "[bases][legendre_polynomials]"

ALEA_TEST_CASE( "Evaluation of Legendre Polynomials",
    BASE_TAGS "[evaluate]" )
{
    ArrayXd expect( 3, 4 );
    expect <<
    1, 1, 1, 1,
    1, 2, 3, 4,
    1, 5.5, 13, 23.5;
    test_polynomial_system_eval( LegendrePolynomials(), expect );
}

ALEA_TEST_CASE( "Norm of Legendre Polynomials", BASE_TAGS "[norm]" )
{
    Array1d expect( 4 );
    expect << 1 / 1.0, 1 / 3.0, 1 / 5.0, 1 / 7.0;
    test_polynomial_system_norm( LegendrePolynomials(), expect.sqrt() );
}

ALEA_TEST_CASE( "Properties of Legendre polynomials",
    BASE_TAGS "[properties]" )
{
    LegendrePolynomials polys;
    test_poly_properties( polys, 10 );
}
