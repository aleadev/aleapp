/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file test_hermite_polynomials.cpp
 * @brief Unit tests for Bases/hermite_polynomials.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include "alea_unittest.hpp"

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/Bases/hermite_polynomials.hpp"
#include "polynomial_test_utils.hpp"

using namespace Alea;

#define BASE_TAGS "[bases][hermite_polynomials]"

ALEA_TEST_CASE( "Recurrence coefficients of Hermite Polynomials",
    BASE_TAGS "[recur_coeff]" )
{
    Array3d rc = HermitePolynomials().recur_coeffs( 4 );
    Array3d expect( 4, 3 );
    expect <<
    0, 1, 0,
    0, 1, 1,
    0, 1, 2,
    0, 1, 3;

    REQUIRE_THAT( rc, Equals( expect ) );
}

ALEA_TEST_CASE( "Evaluation of Hermite Polynomials",
    BASE_TAGS "[hermite][evaluate]" )
{
    ArrayXd expect( 3, 4 );
    expect <<
    1, 1, 1,  1,
    1, 2, 3,  4,
    0, 3, 8, 15;
    test_polynomial_system_eval( HermitePolynomials(), expect );
}

ALEA_TEST_CASE( "Evaluation of Physicists' Hermite Polynomials",
    BASE_TAGS "[physicist][evaluate]" )
{
    // Mathematica formula: Table[HermiteH[2, x], {x, 1, 4}]
    ArrayXd expect( 3, 4 );
    expect <<
    1,  1,  1,  1,
    2,  4,  6,  8,
    2, 14, 34, 62;
    test_polynomial_system_eval( HermitePolynomials( false ), expect );
}

ALEA_TEST_CASE( "Norm of Hermite Polynomials", BASE_TAGS "[norm]" )
{
    Array1d expect( 7 );
    expect << 1, 1, 2, 6, 24, 120, 720;
    test_polynomial_system_norm( HermitePolynomials(), expect.sqrt() );

    test_poly_norm_consistency( HermitePolynomials(), 10 );
}

ALEA_TEST_CASE( "Properties of Hermite polynomials",
    BASE_TAGS "[properties]" )
{
    HermitePolynomials polys_prob( true );
    test_poly_properties( polys_prob, 10 );

    HermitePolynomials polys_phys( false );
    test_poly_properties( polys_phys, 10 );
}
