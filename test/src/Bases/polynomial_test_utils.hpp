/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file test_polynomials_helpers.hpp
 * @brief Header file for 
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#ifndef ALEA_POLYNOMIAL_TEST_UTILS_HPP
#define ALEA_POLYNOMIAL_TEST_UTILS_HPP

namespace Alea {

class PolynomialSystem;
void test_poly_properties(const PolynomialSystem& polys, int n);

void test_polynomial_system_eval( const PolynomialSystem& polys,
    const ArrayXd& expect );

void test_polynomial_system_norm( const PolynomialSystem& polys,
    const Array1d& expect );

void test_poly_norm_consistency( const PolynomialSystem& polys, int n);

} // namespace Alea

#endif // ALEA_POLYNOMIAL_TEST_UTILS_HPP
