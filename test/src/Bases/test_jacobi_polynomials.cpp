/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file test_jacobi_polynomials.cpp
 * @brief Unit tests for Bases/jacobi_polynomials.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include "alea_unittest.hpp"

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/Bases/jacobi_polynomials.hpp"
#include "polynomial_test_utils.hpp"

using namespace Alea;

#define BASE_TAGS "[bases][jacobi_polynomials]"

ALEA_TEST_CASE( "Evaluation of Jacobi Polynomials", BASE_TAGS "[evaluate]" )
{
    ArrayXd expect( 3, 4 );
    expect <<
    1, 1, 1, 1,
    1, 2, 3, 4,
    -0.28125, 1.125, 3.46875, 6.75;
    test_polynomial_system_eval( JacobiPolynomials( 1, 1 ), expect );

    expect <<
    1, 1, 1, 1,
    1, 2.5, 4, 5.5,
    -1.0 / 3, 25.0 / 18, 43.0 / 9, 177.0 / 18;
    test_polynomial_system_eval( JacobiPolynomials( 0, 1 ), expect );

    expect <<
    1, 1, 1, 1,
    3, 4, 5, 6,
    147.0 / 32, 131.0 / 16, 407.0 / 32, 291.0 / 16;
    test_polynomial_system_eval( JacobiPolynomials( 2, 0 ), expect );
}

ALEA_TEST_CASE( "Properties of Jacobi polynomials",
    BASE_TAGS "[properties]" )
{
    JacobiPolynomials polys1( 0.3, 0.6 );
    test_poly_properties( polys1, 10 );

    JacobiPolynomials polys2( 1.3, 2.5 );
    test_poly_properties( polys2, 10 );
}
