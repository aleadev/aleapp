/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file test_monomials.cpp
 * @brief Unit tests for Bases/monomials.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include "alea_unittest.hpp"

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/Bases/monomials.hpp"
#include "polynomial_test_utils.hpp"

using namespace Alea;

#define BASE_TAGS "[bases][monomials]"

ALEA_TEST_CASE( "Evaluation of Monomials", BASE_TAGS "[evaluate]" )
{
    ArrayXd expect( 3, 4 );
    expect <<
    1, 1, 1, 1,
    1, 2, 3, 4,
    1, 4, 9, 16;
    test_polynomial_system_eval( Monomials(), expect );
}

ALEA_TEST_CASE( "Properties of monomials",
    BASE_TAGS "[properties]" )
{
    Monomials polys;
    test_poly_properties( polys, 10 );
}
