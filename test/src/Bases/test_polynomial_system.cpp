/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file test_polynomial_system.cpp
 * @brief Unit tests for Bases/polynomial_system.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include "alea_unittest.hpp"

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/Bases/polynomial_system.hpp"

using namespace Alea;

#define BASE_TAGS "[bases][polynomial_system]"

class DummyPolys : public PolynomialSystem {
public:
    Array3d recur_coeffs( int p ) const
    {
        Array3d rc = Array3d::Ones( p, 3 );
        return rc;
    }
    virtual bool is_monic() const { return true; }
    virtual bool is_normalised() const { return true; }
    virtual bool is_orthogonal() const {return true; }
};

ALEA_TEST_CASE( "Sizes of returned values of PolynomialSystem functions",
    BASE_TAGS "[sizes]" )
{
    DummyPolys polys;

    Array3d rc = polys.recur_coeffs( 5 );
    REQUIRE( rc.cols() ==  3 );
    REQUIRE( rc.rows() ==  5 );

    Array1d norm = polys.norm( 5 );
    REQUIRE( norm.rows() ==  6 );

    Array1d xi( 3 );
    xi << 4, 5, 7;
    ArrayXd eval = polys.evaluate( 17, xi );
    REQUIRE( eval.rows() == 18 );
    REQUIRE( eval.cols() == 3 );
}
