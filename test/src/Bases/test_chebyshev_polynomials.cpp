/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file test_chebyshev_polynomials.cpp
 * @brief Unit tests for Bases/chebyshev_polynomials.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include "alea_unittest.hpp"

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/Bases/chebyshev_polynomials.hpp"

#include "polynomial_test_utils.hpp"

using namespace Alea;

#define BASE_TAGS "[bases][chebyshev_polynomials]"

ALEA_TEST_CASE( "Evaluation of ChebyshevT polynomials (p=4)",
    BASE_TAGS "[chebyshevt][evaluate]" )
{
    ArrayXd expect( 4, 4 );
    expect <<
    1,  1,  1,   1,
    1,  2,  3,   4,
    1,  7, 17,  31,
    1, 26, 99, 244;
    test_polynomial_system_eval( ChebyshevTPolynomials(), expect );
}

ALEA_TEST_CASE( "Properties of ChebyshevT polynomials",
    BASE_TAGS "[chebyshevt][properties]" )
{
    ChebyshevTPolynomials polys;
    test_poly_properties( polys, 10 );
}

ALEA_TEST_CASE( "Evaluation of ChebyshevU polynomials",
    BASE_TAGS "[chebyshevu][evaluate]" )
{
    ArrayXd expect( 4, 4 );
    expect <<
    1,  1,   1,   1,
    2,  4,   6,   8,
    3, 15,  35,  63,
    4, 56, 204, 496;
    test_polynomial_system_eval( ChebyshevUPolynomials(), expect );
}

ALEA_TEST_CASE( "Properties of ChebyshevU polynomials",
    BASE_TAGS "[chebyshevu][properties]" )
{
    ChebyshevUPolynomials polys;
    test_poly_properties( polys, 10 );
}
