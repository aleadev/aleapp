/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file polynomial_algs.cpp
 * @brief Implementation file for polynomial_test_utils.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/Bases/polynomial_system.hpp"
#include "Alea/src/MathUtil/polynomial_algs.hpp"

#include "alea_unittest.hpp"

namespace Alea {

void test_poly_properties( const PolynomialSystem& polys, int n )
{
    ArrayXd rc = polys.recur_coeffs( n );
    REQUIRE( polys.is_monic() == ( rc.col( 1 ) == 1 ).all() );

    Array1d poly_norm = poly_square_norm( rc );
    INFO( "norm of polys:" << poly_norm.transpose() );
    bool seems_orthgonal = ( poly_norm > 0 ).all();
    REQUIRE( polys.is_orthogonal() == seems_orthgonal );

    // That the square_norm is positive is equivalent to Favard's theorem
    bool seems_normalised = ( ( poly_norm - 1.0 ).abs() < 1e-10 ).all();
    REQUIRE( polys.is_normalised() == seems_normalised );
}

void test_polynomial_system_eval( const PolynomialSystem& polys,
        const ArrayXd& expected_values )
{
    Array1d xi( 4 );
    xi << 1, 2, 3, 4;
    int p = static_cast<int>( expected_values.rows() ) - 1;

    ArrayXd y = polys.evaluate( p, xi );

    REQUIRE_THAT( y, CloseTo( expected_values ) );
}

void test_polynomial_system_norm( const PolynomialSystem& polys,
        const Array1d& expected_norm )
{
    int p = static_cast<int>( expected_norm.size() ) - 1;
    Array1d y = polys.norm( p );

    REQUIRE_THAT( y, CloseTo( expected_norm ) );
}

void test_poly_norm_consistency( const PolynomialSystem& polys, int n )
{
    Array3d rc = polys.recur_coeffs( n + 1 );
    Array1d expected_norm = poly_square_norm( rc ).sqrt();
    REQUIRE_THAT( polys.norm( n ), CloseTo( expected_norm ) );
}

} // namespace Alea
