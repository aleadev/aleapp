/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file test_laguerre_polynomials.cpp
 * @brief Unit tests for Bases/laguerre_polynomials.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include "alea_unittest.hpp"

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/Bases/laguerre_polynomials.hpp"
#include "polynomial_test_utils.hpp"

using namespace Alea;

#define BASE_TAGS "[bases][laguerre_polynomials]"

ALEA_TEST_CASE( "Evaluation of Laguerre Polynomials",
    BASE_TAGS "[evaluate]" )
{
    ArrayXd expect( 3, 4 );
    expect <<
    1, 1, 1, 1,
    0, -1, -2, -3,
    -0.5, -1, -0.5, 1;
    test_polynomial_system_eval( LaguerrePolynomials( 0.0 ), expect );
}

ALEA_TEST_CASE( "Properties of Laguerre polynomials",
    BASE_TAGS "[properties]" )
{
    LaguerrePolynomials polys1( 1.0 );
    test_poly_properties( polys1, 10 );

    LaguerrePolynomials polys2( 2.3 );
    test_poly_properties( polys2, 10 );
}
