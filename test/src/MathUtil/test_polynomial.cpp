/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file test_polynomial.cpp
 * @brief Unit tests for MathUtil/polynomial.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include "alea_unittest.hpp"

#include <Alea/Common>
#include <Alea/Types>

struct PolyTester;
#define ALEA_TESTING_HOOK friend struct ::PolyTester;
#include "Alea/src/MathUtil/polynomial.hpp"
using namespace Alea;

struct PolyTester {
    static const Array1d& get_coeffs( const Polynomial& p ) { return p.m_coeffs; };
};

#define BASE_TAGS "[mathutil][polynomial]"

template <typename Derived, typename Created, typename Basetype>
struct Initialiser {
    vector<Basetype> m_values;
    Derived& append( Basetype value )
    {
        m_values.push_back( value );
        return *static_cast<Derived*>( this );
    }
    Created create() const { return Derived::create_impl( m_values ); }
    void restart()
    {
        m_values.resize( 0 );
    }
    Derived& operator<<( Basetype value ) { restart(); return append( value ); }
    Derived& operator,( Basetype value ) { return append( value ); }
    operator Created() const { return create(); }
    Created operator()() const { return create(); }
    friend ostream& operator<<( ostream& os, const Derived& P )
    {
        return os << ( P.create() );
    }

};
struct PolyInit : Initialiser<PolyInit, Polynomial, double> {
    static Polynomial create_impl( const vector<double>& values )
    {
        return Polynomial( values.data(), static_cast<int>( values.size() ) );
    }
};

ALEA_TEST_CASE( "Polynomial constructors and basic functions (size and [])",
    BASE_TAGS "[ctors]" )
{
    SECTION( "Zero polynomial" ) {
        Polynomial p;
        REQUIRE( PolyTester::get_coeffs( p ).size() == 0 );
        REQUIRE( p.size() == 0 );
        REQUIRE( p.degree() == -1 );
        REQUIRE( p[0] == Approx( 0 ) );
        REQUIRE( p[1] == 0 );
    }

    SECTION( "Nonzero polynomial" ) {
        Array1d coeff( 3 );
        coeff << 1, 3, 4;
        Polynomial p( coeff );
        REQUIRE( PolyTester::get_coeffs( p ).size() == 3 );
        REQUIRE( p.size() == 3 );
        REQUIRE( p.degree() == 2 );
        REQUIRE( p[0] == 1 );
        REQUIRE( p[1] == 3 );
        REQUIRE( p[2] == 4 );
        REQUIRE( p[3] == 0 );
    }

    SECTION( "Additional zeros" ) {
        Array1d coeff( 6 );
        coeff << 1, 3, 4, 0, 0, 0;
        Polynomial p( coeff );
        REQUIRE( p.degree() == 2 );
    }
}

ALEA_TEST_CASE( "Evaluation of Empty Polynomial", BASE_TAGS "[evaluate]" )
{
    SECTION( "Empty polynomial" ) {
        Polynomial p;

        REQUIRE( p.size() == 0 );
        REQUIRE( p.evaluate( 0 ) == 0.0 );
        REQUIRE( p.evaluate( 3 ) == 0.0 );
    }
    SECTION( "Non-empty polynomial" ) {
        Array1d coeffs( 3 ); coeffs << 1.0, 2.0, 3.0;
        Polynomial p( coeffs );

        REQUIRE( p.size() == 3 );
        REQUIRE( p.evaluate( 0 ) == 1.0 );
        REQUIRE( p.evaluate( 1 ) == 6.0 );
        REQUIRE( p.evaluate( 2 ) == 17.0 );
    }
}

ALEA_TEST_CASE( "Named constructors (zero, const, x)",
    BASE_TAGS "[named_ctors]" )
{
    SECTION( "Zero polynomial" ) {
        Polynomial p = Polynomial::zero();
        REQUIRE( p.degree() == -1 );
        REQUIRE( p.evaluate( 1 ) == 0.0 );
        REQUIRE( p.evaluate( 3 ) == 0.0 );
    }

    SECTION( "Constant polynomial" ) {
        Polynomial p = Polynomial::constant( 6 );
        REQUIRE( p.degree() == 0 );
        REQUIRE( p.evaluate( 1 ) == 6 );
        REQUIRE( p.evaluate( 3 ) == 6 );
    }

    SECTION( "Zero polynomial" ) {
        Polynomial p = Polynomial::x();
        REQUIRE( p.degree() == 1 );
        REQUIRE( p.evaluate( 1 ) == 1.0 );
        REQUIRE( p.evaluate( 3 ) == 3.0 );
    }
}

ALEA_TEST_CASE( "Test the from_roots method", BASE_TAGS "[from_roots]" )
{
    Array1d r( 3 ); r << 1, 2, 4;
    Array1d c( 4 ); c << -8, 14, -7, 1;
    Polynomial p = Polynomial::from_roots( r );
    REQUIRE( p == Polynomial( c ) );

    Polynomial x = Polynomial::x();
    Polynomial q = ( x - 1 ) * ( x - 2 ) * ( x - 4 );
    REQUIRE( p == q );

    REQUIRE( p.evaluate( 1 ) == 0 );
    REQUIRE( p.evaluate( 2 ) == 0 );
    REQUIRE( p.evaluate( 4 ) == 0 );

    p = Polynomial::from_roots( r, 3.0 );
    REQUIRE( p == Polynomial( 3 * c ) );
}

ALEA_TEST_CASE( "Test the derivative and integrate methods",
    BASE_TAGS "[differentiate][integrate]" )
{
    PolyInit P, Q;
    const Polynomial p0;
    const Polynomial p1 = ( P << 4 );
    const Polynomial p2a = ( P << 0, 4 );
    const Polynomial p2b = ( P << 0, 4 );

    REQUIRE( p0.differentiate() == p0 );
    REQUIRE( ( P << 4 )().differentiate() == p0 );
    REQUIRE( ( P << 3, 5, 7, 2 )().differentiate() == ( Q << 5, 14, 6 ) );

    REQUIRE( p0.integrate() == p0 );
    REQUIRE( p0.integrate( 4 ) == p1 );
}

ALEA_TEST_CASE( "Comparison Operators",
    BASE_TAGS "[operators][operator==][operator!=]" )
{
    PolyInit P, End;
    const Polynomial p0;
    const Polynomial p1 = P << 4;
    const Polynomial q1 = P << 4;
    const Polynomial p3 = ( P << 1, 2, 3 );
    const Polynomial q3 = ( P << 1, 2, 4 );
    const Polynomial r3 = ( P << 5, 2, 3 );

    REQUIRE( p0 == p0 );
    REQUIRE( p1 == q1 );
    REQUIRE( p1 == ( P << 4, 0 ) );
    REQUIRE( p3 == p3 );
    REQUIRE_FALSE( p0 == p3 );
    REQUIRE_FALSE( p1 == p3 );
    REQUIRE_FALSE( p3 == q3 );
    REQUIRE_FALSE( p3 == r3 );

    REQUIRE_FALSE( p0 != p0 );
    REQUIRE_FALSE( p1 != q1 );
    REQUIRE_FALSE( p3 != p3 );
    REQUIRE( p0 != p3 );
}

ALEA_TEST_CASE( "Unary Operators",
    BASE_TAGS "[operators][operator+][operator-]" )
{
    PolyInit P;

    Polynomial z;
    Polynomial p = ( P << 5 );
    Polynomial mp = ( P << -5 );
    Polynomial r = ( P << 3, 7, 3 );
    Polynomial mr = ( P << -3, -7, -3 );

    REQUIRE( p == +p );
    REQUIRE( mp == -p );
    REQUIRE( p == -mp );
    REQUIRE( r == +r );
    REQUIRE( mr == -r );
    REQUIRE( r == -mr );
}

ALEA_TEST_CASE( "Additive Operators",
    BASE_TAGS "[operators][operator+][operator+=]" )
{
    PolyInit P;

    Polynomial z, tmp;
    Polynomial p = ( P << 5 );
    Polynomial q = ( P << 4, 2 );
    Polynomial r = ( P << 3, 7, 3 );
    Polynomial pq = ( P << 9, 2 );
    Polynomial pr = ( P << 8, 7, 3 );
    Polynomial qr = ( P << 7, 9, 3 );

    // Inplace add
    tmp = z;
    tmp += q;
    REQUIRE( tmp == q );
    tmp += r;
    REQUIRE( tmp == qr );
    tmp = r;
    tmp += 5;
    REQUIRE( tmp == pr );
    tmp = z;
    tmp += 5;
    REQUIRE( tmp == p );

    // Add two polynomials
    REQUIRE( pq == p + q );
    REQUIRE( pq == q + p );
    REQUIRE( pr == p + r );
    REQUIRE( pr == r + p );
    REQUIRE( qr == q + r );
    REQUIRE( qr == r + q );
    REQUIRE( r == z + r );
    REQUIRE( r == r + z );
    REQUIRE( z == z + z );

    // Add polynomial and double
    REQUIRE( p == z + 5 );
    REQUIRE( z == p + ( -5 ) );
    REQUIRE( p == 5 + z );
    REQUIRE( pq == 5 + q );
    REQUIRE( pq == q + 5 );
    REQUIRE( pr == 5 + r );
    REQUIRE( pr == r + 5 );
}

ALEA_TEST_CASE( "Subtractive Operators",
    BASE_TAGS "[operators][operator-][operator-=]" )
{
    PolyInit P;

    Polynomial z, tmp;
    Polynomial p = ( P << 5 );
    Polynomial q = ( P << 4, 2 );
    Polynomial r = ( P << 3, 7, 3 );
    Polynomial pq = ( P << 9, 2 );
    Polynomial pr = ( P << 8, 7, 3 );
    Polynomial qr = ( P << 7, 9, 3 );

    // Inplace subtract
    tmp = qr;
    REQUIRE( tmp == qr );
    tmp -= r;
    REQUIRE( tmp == q );
    tmp -= q;
    REQUIRE( tmp == z );
    tmp = pr;
    tmp -= 5;
    REQUIRE( tmp == r );

    // Subtract two polynomials
    REQUIRE( p == pq - q );
    REQUIRE( q == pq - p );
    REQUIRE( p == pr - r );
    REQUIRE( r == pr - p );
    REQUIRE( q == qr - r );
    REQUIRE( r == qr - q );
    REQUIRE( z == r - r );
    REQUIRE( r == r - z );
    REQUIRE( z == z - z );

    // Subtract polynomial and double
    REQUIRE( z == p - 5 );
    REQUIRE( z == 5 - p );
    REQUIRE( q == pq - 5 );
    REQUIRE( r == pr - 5 );
}

ALEA_TEST_CASE( "Multiplicative Operators",
    BASE_TAGS "[operators][operator*][operator*=]" )
{
    PolyInit P;

    Polynomial z, tmp;
    Polynomial one = ( P << 1 );
    Polynomial p = ( P << 5 );
    Polynomial q = ( P << 4, 2 );
    Polynomial r = ( P << 3, 7, 3 );
    Polynomial pq = ( P << 20, 10 );
    Polynomial pr = ( P << 15, 35, 15 );
    Polynomial qr = ( P << 12, 34, 26, 6 );

    // Inplace multiply
    tmp = one;
    tmp *= q;
    REQUIRE( tmp == q );
    tmp *= r;
    REQUIRE( tmp == qr );
    tmp = q;
    tmp *= 5;
    REQUIRE( tmp == pq );

    // Multiply two polynomials
    REQUIRE( pq == p * q );
    REQUIRE( pq == q * p );
    REQUIRE( pr == p * r );
    REQUIRE( pr == r * p );
    REQUIRE( qr == q * r );
    REQUIRE( qr == r * q );
    REQUIRE( z == z * r );
    REQUIRE( z == r * z );
    REQUIRE( z == z * z );
    REQUIRE( r == one * r );
    REQUIRE( r == r * one );

    // Multiply polynomial and double
    REQUIRE( z == z * 5 );
    REQUIRE( z == 5 * z );
    REQUIRE( p == one * 5 );
    REQUIRE( p == 5 * one );
    REQUIRE( pq == 5 * q );
    REQUIRE( pq == q * 5 );
    REQUIRE( pr == 5 * r );
    REQUIRE( pr == r * 5 );
}

ALEA_TEST_CASE( "Division Operators",
    BASE_TAGS "[operators][operator/][operator/=]" )
{
    PolyInit P;

    Polynomial z, tmp;
    Polynomial p = ( P << 1 );
    Polynomial q = ( P << 4, 2 );
    Polynomial r = ( P << 3, 7, 3 );
    Polynomial p5 = ( P << 5 );
    Polynomial q5 = ( P << 20, 10 );
    Polynomial r5 = ( P << 15, 35, 15 );
    Polynomial r20 = ( P << 60, 140, 60 );

    // Inplace divide
    tmp = r20;
    tmp /= 4;
    REQUIRE( tmp == r5 );
    tmp /= 5;
    REQUIRE( tmp == r );

    // Divide polynomial by double
    REQUIRE( z == z / 5 );
    REQUIRE( p == p5 / 5 );
    REQUIRE( q == q5 / 5 );
    REQUIRE( r == r5 / 5 );
}
