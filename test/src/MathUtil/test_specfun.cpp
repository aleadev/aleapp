/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file test_specfun.cpp
 * @brief Unit tests for MathUtil/specfun.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include "alea_unittest.hpp"

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/MathUtil/specfun.hpp"

using namespace Alea;

#define BASE_TAGS "[mathutil][specfun]"

ALEA_TEST_CASE( "Factorial function", BASE_TAGS "[factorial]" )
{
    REQUIRE( factorial( 0 ) == 1 );
    REQUIRE( factorial( 1 ) == 1 );
    REQUIRE( factorial( 2 ) == 2 );
    REQUIRE( factorial( 3 ) == 6 );
    REQUIRE( factorial( 5 ) == 120 );
    REQUIRE( factorial( 10 ) == 3628800 );
}

ALEA_TEST_CASE( "Binomial function", BASE_TAGS "[binomial]" )
{
    REQUIRE( binomial( 0, 0 ) == 1 );
    REQUIRE( binomial( 1, 0 ) == 1 );
    REQUIRE( binomial( 4, 0 ) == 1 );
    REQUIRE( binomial( 3, 3 ) == 1 );
    REQUIRE( binomial( 2, 2 ) == 1 );

    REQUIRE( binomial( 7, 1 ) == 7 );
    REQUIRE( binomial( 7, 2 ) == 21 );
    REQUIRE( binomial( 7, 3 ) == 35 );

    REQUIRE( binomial( 13, 3 ) == 13 * 2 * 11 );
}
