/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file test_multiindex.cpp
 * @brief Unit tests for MathUtil/multiindex.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include "alea_unittest.hpp"

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/MathUtil/multiindex.hpp"

using namespace Alea;

#define BASE_TAGS "[mathutil][multiindex]"

ALEA_TEST_CASE( "Create complete set (m=2, p=0)", BASE_TAGS "[complete_set]" )
{
    MultiindexSet set = MultiindexSet::create_complete_set( 2, 0 );

    MultiindexArray m( 1, 2 );
    m <<
    0, 0;

    MultiindexArray values = set.get_values();
    REQUIRE_THAT( values, Equals( m ) );
}

ALEA_TEST_CASE( "Create complete set (m=2, p=2)", BASE_TAGS "[complete_set]" )
{
    MultiindexSet set = MultiindexSet::create_complete_set( 2, 2 );

    MultiindexArray m( 6, 2 );
    m <<
    0, 0,
    0, 1,
    1, 0,
    0, 2,
    1, 1,
    2, 0;

    MultiindexArray values = set.get_values();
    REQUIRE_THAT( values, Equals( m ) );
}
