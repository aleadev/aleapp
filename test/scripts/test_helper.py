import sys
import os

inf=float("Inf")

def format_number(x):
    if x==inf:
        return "INFINITY"
    elif x==-inf:
        return "-INFINITY"
    else:
        return "{x:.11g}".format(x=x)

def format_list(arr):
    return ", ".join(format_number(x) for x in arr)

def print_eigen_init(arr, varname, etype=None):
    if etype is not None:
        retval = "{etype} {varname}({sz});\n".format(
            etype=etype, varname=varname, sz=len(arr))
    else:
        retval = ""
    retval +="{varname} << ".format(varname=varname, sz=len(arr))
    retval += ", ".join([format_number(x) for x in arr])
    retval += ";"
    print retval

def print_eigen_init_mat(arr, varname):
    print_eigen_init(arr, varname, "Matrix")

    
def underline(s):
  print s
  print "="*len(s)




from string import Template
import re

def subst(str, **kwds):
    substs=globals().copy()
    substs.update(kwds)
    return Template(str).substitute(substs)
  
  
class TestGenerator:
    def __init__(self, filename, scriptname):
        self.filename = filename
        self.fullfile = os.path.normpath(os.getcwd() + "/" + "../src/" + filename)
        self.content = ""
        self.scriptname = scriptname
        self.start_phrase = "// The test cases below were automatically generated by $script. Do not edit."
        self.end_phrase = "// The test cases above were automatically generated by $script. Do not edit."
        
    def addContent(self, str):
        self.content += str
        
    def readOldContent(self):
        fullpath = self.fullfile
        does_exist = os.path.isfile(fullpath)
        precont = ""
        postcont = ""

        if not does_exist:
            return (precont, postcont)
        
        with open(fullpath, 'r') as f:
            old_content = f.read()
        
        lines = old_content.split("\n")
        curr = 0
        regstr = subst(self.start_phrase, script=".*")

        prog = re.compile(regstr)
        while curr < len(lines):
            if prog.match(lines[curr]):
                break;
            precont = precont + lines[curr] + "\n"
            curr = curr + 1

        regstr = subst(self.end_phrase, script=".*")
        prog = re.compile(regstr)
        while curr < len(lines):
            if prog.match(lines[curr]):
                curr = curr + 1
                break;
            curr = curr + 1
            
        while curr < len(lines):
            postcont = postcont + lines[curr] + "\n"
            curr = curr + 1
        
        return (precont, postcont)
        

        
    def writeTest(self):
        (precont, postcont) = self.readOldContent()
        script=os.path.basename(self.scriptname)
        new_content = (precont 
                       + subst(self.start_phrase, script=script)
                       + "\n"
                       + self.content
                       + subst(self.end_phrase, script=script)
                       + postcont)
                
        fullpath = self.fullfile
        with open(fullpath, 'w') as f:
            f.write(new_content)
            
        from subprocess import call
        call(["../../scripts/kdev_format_source", fullpath])
