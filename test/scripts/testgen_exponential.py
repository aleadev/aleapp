#!/usr/bin/env python

from test_helper_dists import *
import scipy.stats as stats

filename = "Statistics/test_exponential_distribution.cpp"
testgen = TestGenerator( filename, __file__)


name="Exponential"
dist=stats.expon
x = [-inf, -3, 0.0, 0.2, 0.4, 0.7, 1.0, 2.2, 5, inf]
y = [0.0, 0.001, 0.2, 0.4, 0.7, 0.999, 1.0]

test_number = 1
lambda_ = 1
params=[lambda_]
scipy_params = [0, 1.0/lambda_]
testgen.addContent(stats_test(dist(*scipy_params), x, y, name, params, number=test_number))

test_number = 2
lambda_ = 2.3
params=[lambda_]
scipy_params = [0, 1.0/lambda_]
testgen.addContent(stats_test(dist(*scipy_params), x, y, name, params, number=test_number))


testgen.writeTest()
