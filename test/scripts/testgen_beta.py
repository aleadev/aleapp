#!/usr/bin/env python

from test_helper_dists import *
from scipy.stats import beta

filename = "Statistics/test_beta_distribution.cpp"
testgen = TestGenerator( filename, __file__)


name="Beta"
dist=beta
x = [-inf, 0.0, 0.2, 0.4, 0.7, 1.0, inf]
y = [0.0, 0.2, 0.4, 0.7, 1.0]

test_number = 1
a = 2.15
b = 3.29
params = [a, b]
scipy_params = params
testgen.addContent(stats_test(dist(*scipy_params), x, y, name, params, number=test_number))

test_number = 2
a = 0.37
b = 0.59
params = [a, b]
scipy_params = params
testgen.addContent(stats_test(dist(*scipy_params), x, y, name, params, number=test_number))


testgen.writeTest()
