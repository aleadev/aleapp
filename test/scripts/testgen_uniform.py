#!/usr/bin/env python

from test_helper_dists import *
from scipy.stats import uniform

filename = "Statistics/test_uniform_distribution.cpp"
testgen = TestGenerator( filename, __file__)


name="Uniform"
dist=uniform
x = [-inf, -0.3, -0.2, 0.0, 0.2, 0.4, 0.7, 1.0, 10.0, inf]
y = [0.0, 0.001, 0.2, 0.4, 0.7, 1.0]

test_number = 1
a = 0
b = 1
params=[a, b]
scipy_params = [a, b-a]
testgen.addContent(stats_test(dist(*scipy_params), x, y, name, params, number=test_number))


test_number = 2
a = -0.21
b = 3.29
params=[a, b]
scipy_params = [a, b-a]
testgen.addContent(stats_test(dist(*scipy_params), x, y, name, params, number=test_number))


testgen.writeTest()
