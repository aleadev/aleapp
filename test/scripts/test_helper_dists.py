import os.path
from test_helper import *

def print_stats_old(dist, x, y):
  underline("pdf")
  print_eigen_init(x, "x")
  print_eigen_init(dist.pdf(x), "expect")
  print

  underline("cdf")
  print_eigen_init(x, "x")
  print_eigen_init(dist.cdf(x), "expect")
  print

  underline("invcdf")
  print_eigen_init(y, "y")
  print_eigen_init(dist.ppf(y), "expect")

def stats_test(dist, x, y, name, params, reltol=None, number=None):
  strparams = ", ".join(str(p) for p in params)
  
  return """
ALEA_TEST_CASE( "Moments of {dname} distribution (Array1d{Number})",
    BASE_TAGS "[pdf]" )
{{
    {dname}Distribution dist( {params} );
    Array1d actual( 4 ), expect( 4 );
    actual << dist.mean(), dist.var(), dist.skewness(), dist.kurtosis_excess();
    expect << {moments};
    REQUIRE_THAT( actual, CloseTo( expect{Delta} ) );
}}

ALEA_TEST_CASE( "PDF of {dname} distribution (Array1d{Number})",
    BASE_TAGS "[pdf]" )
{{
    {dname}Distribution dist( {params} );
    Array1d x( {lenx} ), expect( {lenx} );
    x << {x};
    expect << {pdfx};
    REQUIRE_THAT( dist.pdf( x ), CloseTo( expect{Delta} ) );
}}

ALEA_TEST_CASE( "CDF of {dname} distribution (Array1d{Number})",
    BASE_TAGS "[cdf]" )
{{
    {dname}Distribution dist( {params} );
    Array1d x( {lenx} ), expect( {lenx} );
    x << {x};
    expect << {cdfx};
    REQUIRE_THAT( dist.cdf( x ), CloseTo( expect{Delta} ) );
}}

ALEA_TEST_CASE( "INVCDF of {dname} distribution (Array1d{Number})",
    BASE_TAGS "[invcdf]" )
{{
    {dname}Distribution dist( {params} );
    Array1d y( {leny} ), expect( {leny} );
    y << {y};
    expect << {invcdfy};
    REQUIRE_THAT( dist.invcdf( y ), CloseTo( expect{Delta} ) );
}}

""".format(dname=name, 
	   lname=name.lower(), 
	   params=strparams, 
	   moments=format_list([dist.mean(), dist.var(), float(dist.stats('s')), float(dist.stats('k'))]),
	   lenx=len(x), 
	   x=format_list(x),
	   pdfx=format_list(dist.pdf(x)),
	   cdfx=format_list(dist.cdf(x)),
	   leny=len(y),
	   y=format_list(y),
	   invcdfy=format_list(dist.ppf(y)),
	   Delta="" if reltol is None else ", " + str(reltol),
	   Number="" if number is None else "/" + str(number)
	   )
