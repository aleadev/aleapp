#!/usr/bin/env python

from test_helper_dists import *
from scipy.stats import norm as normal

filename = "Statistics/test_normal_distribution.cpp"
testgen = TestGenerator( filename, __file__)


name="Normal"
dist=normal
x = [-inf, -3, 0.0, 0.2, 0.4, 0.7, 1.0, 2.2, 5, inf]
y = [0.0, 0.001, 0.2, 0.4, 0.7, 0.999, 1.0]

test_number = 1
mu = 0
sigma = 1
params=[mu, sigma]
scipy_params = params
testgen.addContent(stats_test(dist(*scipy_params), x, y, name, params, number=test_number))

test_number = 2
mu = 2.5
sigma = 1.72
params=[mu, sigma]
scipy_params = params
testgen.addContent(stats_test(dist(*scipy_params), x, y, name, params, number=test_number))


testgen.writeTest()
