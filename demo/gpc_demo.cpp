#include <Alea/GPC>

#include "Alea/src/MathUtil/polynomial_algs.hpp"
#include "Alea/src/MathUtil/numint.hpp"
#include "Alea/src/MathUtil/specfun.hpp"

#include <iostream>

using namespace Alea;

//#define EIGEN_FIX(m,minval) (m.array()*(m.array().abs()>minval).cast<double>())
#define EIGEN_FIX( m, minval ) ( m.array() * \
            ( m.array().abs() > 0 ).cast<double>() )

// Matrix test_orthogonality(const Distribution& dist, const PolynomialSystem& basis, int n ) {
//   Array3d rc = basis.recur_coeffs(n+10);
//   Quad1d rule = Alea::poly::gauss_rule_from_rc(rc);
//   Matrix val = basis.evaluate(n, rule.x);
//   Matrix m = val.transpose() * (val.array().colwise() * (rule.w)).matrix();
//
//   //ALEA_DEBUG_SHOW(m.array().abs());
//   ALEA_DEBUG_SHOWLN(EIGEN_FIX(m, 1e-5));
//
//   return m;
// }

Matrix test_orthogonality_by_mc( const ContinuousDistribution& dist,
    const PolynomialSystem& basis, int n,
    int N = 100000 )
{
    Array1d x = dist.sample( N );
    Matrix val = basis.evaluate( n, x );

    Matrix m = val.transpose() * val / N;
    ALEA_DEBUG_SHOWLN( EIGEN_FIX( m, 1e-2 ) );

    return m;
}

struct BasisProductFunc : UnaryFunc {
    Ref<const ContinuousDistribution> m_dist;
    Ref<const PolynomialSystem> m_basis;
    int i, j;

    BasisProductFunc( Ref<const ContinuousDistribution> dist,
        Ref<const PolynomialSystem> basis ) :
        m_dist( dist ), m_basis( basis ), i( 0 ), j( 0 ) {}

    virtual double eval( double x )
    {
        Array1d x_( 1 );
        x_( 0 ) = x;
        int m = ( i > j ) ? i : j;
        Matrix val = m_basis->evaluate( m + 1, x_ );
        return val( i, 0 ) * val( j, 0 ) * m_dist->pdf( x );
    }
};

Matrix test_orthogonality_by_quad( Ref<const ContinuousDistribution> dist,
    Ref<const PolynomialSystem> basis,
    int n )
{
    Quadrature1d Q;
    Matrix m( n + 1, n + 1 );
    double a = dist->invcdf( 0 );
    double b = dist->invcdf( 1 );

    BasisProductFunc basis_prod_func( dist, basis );
    for( int i = 0; i <= n; i++ ) {
        for( int j = 0; j <= n; j++ ) {
            basis_prod_func.i = i;
            basis_prod_func.j = j;
            m( i, j ) = Q.quad1d( basis_prod_func, a, b );
        }
    }

    ALEA_DEBUG_SHOWLN( EIGEN_FIX( m, 1e-5 ) );

    return m;
}

Matrix test_orthogonality_by_quad( const GPCPair& pair, int n )
{
    return test_orthogonality_by_quad( pair.get_dist(), pair.get_system(), n );
    //return test_orthogonality_by_mc(*pair.get_dist(), *pair.get_system(), n, 1000000);
}

#include "Alea/src/Util/io.hpp"
#include <typeinfo>
#include <cstddef>

int main()
{
    add_stream_extension( std::cout );

    GPCRegistry registry = GPCRegistry::get_default();
    {
        ScopedMinDouble dummy( std::cout, 1e-10 );
        test_orthogonality_by_quad( registry.get_pair( 'H' ), 6 );
        test_orthogonality_by_quad( registry.get_pair( 'P' ), 6 );
        test_orthogonality_by_quad( registry.get_pair( 'L' ), 6 );
    }

    //std::cout << typeid(GPCRegistry).name() << std::endl;
    Distribution* dist = new UniformDistribution( 0, 1 );
    std::cout << typeid( dist ).name() << std::endl;
    std::cout << typeid( Distribution* ).name() << std::endl;

    std::cout << typeid( *dist ).name() << std::endl;
    std::cout << typeid( UniformDistribution ).name() << std::endl;

    std::cout << typeid( Distribution ).name() << std::endl;

    //   std::cout << typeid(GPCRegistry).hash_code() << std::endl;

    /*
     *

     **#include <cstddef>

       Alea::Object {
       virtual ~Object() {};
       size_t hash_value();
       } // maybe in Distribution?

       // macro to conveniently define specialization for a class derived from Hashable
     **#define DEFINE_HASH_SPECIALIZATION(hashable)                               \
       namespace std {                                                                \
       template<>                                                                     \
       struct hash<hashable> {                                                        \
       std::size_t operator()(const hashable& object) const {                       \
       return object.Hash();                                                      \
       }                                                                            \
       };                                                                             \
       }
       DEFINE_HASH_SPECIALIZATION(Distribution);
       Distribution dist::hash_value() { return typeid(




       GPCGerm germ;
       germ.add( make_ref(new NormalDistribution()) );
       germ.add( make_ref(new ExponentialDistribution()) );
       germ.add( make_ref(new ExponentialDistribution()) );
       germ.add( make_ref(new UniformDistribution()) );

       ArrayXd xi = germ.sample( 100 ); // which order?
     **#include <Alea/Qt/SimplePlot>

       SimplePlot plot = SimplePlot::plot_density( xi, 1, 3 )
       plot.title("Normal vs. Exponential");
       plot.wait();



       GPCBasis basis = GPCBasis::from_germ( germ, PolynomialSystem::NORMALISED_POLYNOMIALS ); // vs. MONIC_POLYNOMIALS vs. STANDARD_POLYNOMIALS
       enum PolynomialSystem::Type {
       NORMALISED_POLYNOMIALS,
       MONIC_POLYNOMIALS,
       STANDARD_POLYNOMIALS
       }
       bool PolynomialSystem::is_monic()
       bool PolynomialSystem::is_normalised()
       bool PolynomialSystem::is_complete()
       Ref<PolynomialSystem> to_monic(Ref<PolynomialSystem> polys)
       Ref<PolynomialSystem> to_normalised(Ref<PolynomialSystem> polys)
       Ref<PolynomialSystem> to_type(Ref<PolynomialSystem> polys, Type type)

       CachedPolynomialsSystem : public PolynomialSystem











       // where is the relation Distribution to PolynomialSystem?
       // virtual? Distribution::get_orthogonal_polynomials( MONIC_POLYNOMIALS|NORMALISED_POLYNOMIALS|STANDARD_POLYNOMIALS)?









       }
     */

    GPCGerm germ = registry.create_germ( "HHPLP" );
    return 0;
}
