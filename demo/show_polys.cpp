/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file show_polys.cpp
 * @brief Demonstration of polynomial functions
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 *
 * This file demonstrates how to get explicit representations of polynomials
 * of some systems of orthogonal polynomials, like Hermite, Legendre etc., and
 * how to print them in a formatted way.
 */

#include <Alea/Common>
#include <Alea/Types>
#include <Alea/Polynomials>

#include "Alea/src/MathUtil/polynomial.hpp"
#include "Alea/src/MathUtil/polynomial_algs.hpp"

#include <iostream>

using namespace Alea;

void show_polys( ostream& os, const string& symbol, Ref<PolynomialSystem> polys,
    int p )
{
    Polynomial::IOFormat format = Polynomial::default_format;
    Polynomial::default_format = format.loose().highest_first();

    Array3d rc = polys->recur_coeffs( p );
    vector<Polynomial> v = poly_eval( rc, Polynomial::x() );

    for( int i = 0; i <= p; i++ ) {
        os << symbol << "[" << i << "] = " << v[i] << std::endl;
    }

    Polynomial::default_format = format;
}

int main()
{
    ostream& os = std::cout;

    os << "Hermite" << std::endl;
    show_polys( std::cout, "H", make_ref( new HermitePolynomials ), 5 );
    os << std::endl;

    os << "Legendre" << std::endl;
    show_polys( std::cout, "P", make_ref( new LegendrePolynomials ), 5 );
    os << std::endl;

    os << "ChebyshevT" << std::endl;
    show_polys( std::cout, "T", make_ref( new ChebyshevTPolynomials ), 5 );
    os << std::endl;

    os << "ChebyshevU" << std::endl;
    show_polys( std::cout, "U", make_ref( new ChebyshevUPolynomials ), 5 );
    os << std::endl;

    os << "Laguerre(1)" << std::endl;
    show_polys( std::cout, "L", make_ref( new LaguerrePolynomials( 1 ) ), 5 );
    os << std::endl;

    return 0;
}
