/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file io.cpp
 * @brief Implementation file for Util/io.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/Util/io.hpp"

#include <ostream>

namespace Alea {

// TODO: The manipulators (min_double) are not correctly assigned to the stream they
// modify. This needs to be fixed. Probably the whole concept is kind of flawed. Maybe
// get rid of the stack and use boost::ios stuff.

inline int get_ios_index()
{
    static int i = std::ios_base::xalloc();
    return i;
}

class MinStack {
    std::vector<double> m_min_vec;
    static std::map<int, MinStack> s_min_stacks;
public:
    MinStack()
        : m_min_vec() {}

    double get_min_double() const
    {
        if( m_min_vec.empty() )
            return 0.0;
        else
            return m_min_vec.back();
    }

    static double get_min_double( int i )
    {
        return s_min_stacks[i].get_min_double();
    }

    static void push( int i, double v )
    {
        s_min_stacks[i].m_min_vec.push_back( v );
    }

    static void pop( int i )
    {
        s_min_stacks[i].m_min_vec.pop_back();
    }
};
std::map<int, MinStack> MinStack::s_min_stacks;

std::ostream& operator<<( std::ostream& os, const push_min_double& pmd )
{
    MinStack::push( get_ios_index(), pmd.m_min_double );
    return os;
}

std::ostream& pop_min_double( std::ostream& os )
{
    MinStack::pop( get_ios_index() );
    return os;
}

#ifdef HAVE_NUM_PUT
namespace internal {
struct alea_num_put
    : std::num_put<char> {
    iter_type do_put( iter_type s, std::ios_base& f, char_type fill,
                double v ) const
    {
        double min_double = MinStack::get_min_double( get_ios_index() );
        if ( v >= -min_double && v <= min_double )
            v = 0.0;
        return std::num_put<char>::do_put( s, f, fill, v );
    }
};
}
void add_stream_extension( std::ostream& os )
{
    os.imbue( std::locale( std::locale(), new internal::alea_num_put ) );
}
#else
void add_stream_extension( std::ostream& /* os */ )
{
}
#endif

ScopedMinDouble::ScopedMinDouble( std::ostream& os,
        double min_double )
    : m_os( os )
{
    m_os << push_min_double( min_double );
}

ScopedMinDouble::~ScopedMinDouble()
{
    m_os << pop_min_double;
}
push_min_double::push_min_double ( double min_double )
    : m_min_double(
        min_double ) {}

} // namespace Alea
