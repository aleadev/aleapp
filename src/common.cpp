/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2015 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file common.cpp
 * @brief Implementation file for common.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/common.hpp"

#include <sstream>
#include <stdexcept>
using std::ostream;

namespace Alea {

namespace internal {

#ifdef __GNUC__
#include <execinfo.h>
inline ostream& write_stacktrace( ostream& os )
{
    void * array[50];
    int size = backtrace( array, 50 );

    os << " Backtrace returned " << size << " frames\n\n";

    char ** messages = backtrace_symbols( array, size );

    for ( int i = 0; i < size && messages != NULL; ++i ) {
        os << "[bt]: (" << i << ") " << messages[i] << std::endl;
    }
    os << std::endl;

    free( messages );
    return os;
}
#else
inline ostream& write_stacktrace( ostream& os )
{
    return os;
}
#endif // __GNUC__

void eigen_assert_fail(
            const char *condition,
            const char *function,
            const char *file, int line )
{
    std::stringstream buf;
    buf << "EIGEN ASSERTION FAILED: " << condition << "\nIN FUNCTION: "
        << function << " AT " << file << ":" << line << std::endl;
    // Maybe the exception thrown should be some "eigen_exception"
    // derived from runtime_error.
    write_stacktrace( buf );
    throw ( std::runtime_error( buf.str() ) );
}

} // namespace internal

} // namespace Alea
