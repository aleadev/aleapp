/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2015 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file distribution.cpp
 * @brief Implementation file for Statistics/distribution.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/Statistics/distribution.hpp"

//#include <boost/math/distributions/uniform.hpp>
//using boost::math::uniform_distribution;
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_01.hpp>
using boost::uniform_01;
using boost::mt19937;

namespace Alea {

typedef double ( ContinuousDistribution::*dist_fun_ptr )( double ) const;
static ArrayXd apply_for_all( const ArrayXd& x,
        const ContinuousDistribution* dist,
        dist_fun_ptr fptr )
{
    const double* xv = x.data();
    ArrayXd y( x.rows(), x.cols() );
    double* yv = y.data();
    for( int i = 0; i < x.size(); i++ ) {
        yv[i] = ( dist->*fptr )( xv[i] );
    }
    return y;
}

ArrayXd ContinuousDistribution::cdf( const ArrayXd& x ) const
{
    return apply_for_all( x, this, &ContinuousDistribution::cdf );
}

ArrayXd ContinuousDistribution::pdf( const ArrayXd& x ) const
{
    return apply_for_all( x, this, &ContinuousDistribution::pdf );
}

ArrayXd ContinuousDistribution::invcdf( const ArrayXd& y ) const
{
    return apply_for_all( y, this, &ContinuousDistribution::invcdf );
}

Array1d ContinuousDistribution::sample( int n ) const
{
    mt19937 generator;
    uniform_01<double> dist;
    Array1d res( n );
    for( int i = 0; i < n; i++ )
        res[i] = dist( generator );
    res = invcdf( res );
    return res;
}

} // namespace Alea
