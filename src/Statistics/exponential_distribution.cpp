/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2015 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file exponential_distribution.cpp
 * @brief Implementation file for Statistics/exponential_distribution.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/Statistics/exponential_distribution.hpp"

#include <boost/math/distributions/exponential.hpp>
using boost::math::exponential_distribution;

namespace Alea {

struct ExponentialDistribution::impl {
    exponential_distribution<> dist;

    impl( double lambda )
        : dist( lambda ) {}
};

ExponentialDistribution::ExponentialDistribution( double lambda )
    : m_dist( new impl( lambda ) ) {}

ExponentialDistribution::~ExponentialDistribution()
{}

double ExponentialDistribution::mean() const
{
    return boost::math::mean<>( m_dist->dist );
}

double ExponentialDistribution::var() const
{
    return boost::math::variance<>( m_dist->dist );
}

double ExponentialDistribution::skewness() const
{
    return boost::math::skewness<>( m_dist->dist );
}

double ExponentialDistribution::kurtosis_excess() const
{
    return boost::math::kurtosis_excess<>( m_dist->dist );
}

double ExponentialDistribution::pdf( double x ) const
{
    if( x < 0 )
        return 0.0;
    else
        return boost::math::pdf( m_dist->dist, x );
}

double ExponentialDistribution::cdf( double x ) const
{
    if( x < 0 )
        return 0.0;
    else
        return boost::math::cdf( m_dist->dist, x );
}

double ExponentialDistribution::invcdf( double y ) const
{
    ALEA_ASSERT( 0 <= y && y <= 1 && "y must be in [0,1] for invcdf" );
    if ( y == 1 )
        return INFINITY;
    else
        return boost::math::quantile( m_dist->dist, y );
}

} // namespace Alea
