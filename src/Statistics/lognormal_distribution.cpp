/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2015 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file lognormal_distribution.cpp
 * @brief Implementation file for Statistics/lognormal_distribution.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/Statistics/lognormal_distribution.hpp"

#include <boost/math/distributions/lognormal.hpp>
using boost::math::lognormal_distribution;

namespace Alea {

struct LogNormalDistribution::impl {
    lognormal_distribution<> dist;

    impl( double mu, double sigma )
        : dist( mu, sigma ) {}
};

LogNormalDistribution::LogNormalDistribution( double mu, double sigma )
    : m_dist( new impl( mu, sigma ) ) {}

LogNormalDistribution::~LogNormalDistribution() {}

double LogNormalDistribution::mean() const
{
    return boost::math::mean<>( m_dist->dist );
}

double LogNormalDistribution::var() const
{
    return boost::math::variance<>( m_dist->dist );
}

double LogNormalDistribution::skewness() const
{
    return boost::math::skewness<>( m_dist->dist );
}

double LogNormalDistribution::kurtosis_excess() const
{
    return boost::math::kurtosis_excess<>( m_dist->dist );
}

double LogNormalDistribution::pdf( double x ) const
{
    if( x < 0 )
        return 0;
    else if( isinf( x ) )
        return 0;
    return boost::math::pdf( m_dist->dist, x );
}

double LogNormalDistribution::cdf( double x ) const
{
    if( x < 0 )
        return 0;
    else if( isinf( x ) )
        return 1;
    return boost::math::cdf( m_dist->dist, x );
}

double LogNormalDistribution::invcdf( double y ) const
{
    ALEA_ASSERT( 0 <= y && y <= 1 && "y must be in [0,1] for invcdf" );
    if( y == 0 )
        return 0;
    else if ( y == 1 )
        return INFINITY;
    else
        return boost::math::quantile( m_dist->dist, y );
}

} // namespace Alea
