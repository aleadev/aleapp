/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2015 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file beta_distribution.cpp
 * @brief Implementation file for Statistics/beta_distribution.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/Statistics/beta_distribution.hpp"

#include <boost/math/distributions/beta.hpp>
using boost::math::beta_distribution;
using namespace boost::math::policies;

namespace Alea {

struct BetaDistribution::impl {
    typedef policy<overflow_error<errno_on_error> > no_overflow_policy;
    beta_distribution<double, no_overflow_policy> dist;

    impl( double alpha, double beta )
        : dist( alpha, beta ) {}
};

BetaDistribution::BetaDistribution( double alpha, double beta )
    : m_dist( new impl( alpha, beta ) ) {}

BetaDistribution::~BetaDistribution() {}

double BetaDistribution::mean() const
{
    return boost::math::mean<>( m_dist->dist );
}

double BetaDistribution::var() const
{
    return boost::math::variance<>( m_dist->dist );
}

double BetaDistribution::skewness() const
{
    return boost::math::skewness<>( m_dist->dist );
}

double BetaDistribution::kurtosis_excess() const
{
    return boost::math::kurtosis_excess<>( m_dist->dist );
}

double BetaDistribution::pdf( double x ) const
{
    if( x < 0 || x > 1 ) return 0.0;
    return boost::math::pdf( m_dist->dist, x );
}

double BetaDistribution::cdf( double x ) const
{
    if( x <= 0 ) return 0.0;
    if( x >= 1 ) return 1.0;
    return boost::math::cdf( m_dist->dist, x );
}

double BetaDistribution::invcdf( double y ) const
{
    return boost::math::quantile( m_dist->dist, y );
}

} // namespace Alea
