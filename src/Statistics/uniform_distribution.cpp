/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2015 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file uniform_distribution.cpp
 * @brief Implementation file for Statistics/uniform_distribution.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/Statistics/uniform_distribution.hpp"

namespace Alea {

UniformDistribution::UniformDistribution( double a, double b )
    : m_a( a ), m_b( b ) {}

double UniformDistribution::mean() const
{
    return 0.5 * ( m_a + m_b );
}

double UniformDistribution::var() const
{
    return 1 / 12.0 * ( m_b - m_a ) * ( m_b - m_a );
}

double UniformDistribution::skewness() const
{
    return 0.0;
}

double UniformDistribution::kurtosis_excess() const
{
    return -1.2;
}

double UniformDistribution::pdf( double x ) const
{
    if( x < m_a || x > m_b ) return 0.0;
    return 1.0 / ( m_b - m_a );
}

double UniformDistribution::cdf( double x ) const
{
    if( x <= m_a ) return 0.0;
    if( x >= m_b ) return 1.0;
    return ( x - m_a ) / ( m_b - m_a );
}

double UniformDistribution::invcdf( double y ) const
{
    ALEA_ASSERT( 0 <= y && y <= 1 && "y must be in [0,1] for invcdf" );
    return m_a + ( m_b - m_a ) * y;
}

} // namespace Alea
