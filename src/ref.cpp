/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2015 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file ref.cpp
 * @brief Implementation file for /ref.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src//ref.hpp"

namespace Alea {

namespace internal {

const char* get_ref_error_stored_static_ref()
{
    return \
        "A Ref to a stack allocated object has been passed to an outer scope "
        "or stored permanently, which is not allowed, because the reference "
        "that still exists may not reference the stack object, which is "
        "currently being destroyed.";
}

}

} // namespace Alea
