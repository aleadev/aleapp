/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file polynomial_system.cpp
 * @brief Implementation file for Bases/polynomial_system.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/Bases/polynomial_system.hpp"
#include "Alea/src/MathUtil/polynomial_algs.hpp"

namespace Alea {

ArrayXd PolynomialSystem::evaluate( int p, const Array1d& xi ) const
{
    Array3d rc = this->recur_coeffs( p );
    return poly_eval( rc, xi );
}

Array1d PolynomialSystem::norm( int p ) const
{
    Array3d rc = this->recur_coeffs( p + 1 );
    return poly_square_norm( rc, 1.0 ).sqrt();
}

} // namespace Alea
