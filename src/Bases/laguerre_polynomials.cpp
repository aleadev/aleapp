/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file laguerre_polynomials.cpp
 * @brief Implementation file for Bases/laguerre_polynomials.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/Bases/laguerre_polynomials.hpp"

namespace Alea {

LaguerrePolynomials::LaguerrePolynomials( double alpha )
    : m_alpha( alpha ) {}

Array3d LaguerrePolynomials::recur_coeffs( int p ) const
{
    Array3d rc( p, 3 );
    for( int n = 0; n < p; n++ ) {
        rc( n, 0 ) = ( 2 * n + 1.0 + m_alpha ) / ( n + 1.0 );
        rc( n, 1 ) = -1.0 / ( n + 1.0 );
        rc( n, 2 ) = ( n + m_alpha ) / ( n + 1.0 );
    }
    return rc;
}

bool LaguerrePolynomials::is_monic() const
{
    return false;
}

bool LaguerrePolynomials::is_normalised() const
{
    return m_alpha == 0;
}

bool LaguerrePolynomials::is_orthogonal() const
{
    return true;
}

} // namespace Alea
