/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file polynomial.cpp
 * @brief Implementation file for MathUtil/polynomial.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/MathUtil/polynomial.hpp"

#include <stdexcept>

namespace Alea {

// Constructors

Polynomial::Polynomial()
    : m_coeffs() {}

Polynomial::Polynomial ( const CoeffArray &coeffs )
    : m_coeffs( coeffs ) {}

Polynomial::Polynomial( const CoeffType *coeffs, int n )
    : m_coeffs( Eigen::Map<const Array1d>( coeffs, n ) ) {}

// Named constructors (static)

Polynomial Polynomial::zero()
{
    return Polynomial();
}

Polynomial Polynomial::constant( const CoeffType &c )
{
    return Polynomial() + c;
}

Polynomial Polynomial::x()
{
    return Polynomial( Vector::Unit( 2, 1 ) );
}

Polynomial Polynomial::from_roots( Array1d r, double a_n )
{
    /* The algorithm is equivalent to the following:
     * p_0(x) = x^n
     * p_{i+1}(x) = p_i(x)(1 - r_i/x)
     * Then obviously p_n(x) = (x-r_1)*(x-r_2)*...*(x-r_n)
     */
    int n = static_cast<int>( r.size() );
    Array1d c = Array1d::Zero( n + 1 );

    c[n] = 1;
    for( int i = 0; i < n; i++ ) {
        // In the following line, enforce evaluation by Eigen as otherwise aliasing problems can occur
        // when certain optimisations are activated.
        c.head( n ) -= r[i] * c.tail( n ).eval();
    }
    if( a_n != 1.0 )
        c *= a_n;
    return Polynomial( c );
}

// Basic information and access functions
bool operator==( const Polynomial& p1, const Polynomial& p2 )
{
    int n = p1.degree();
    return n == p2.degree() &&
           ( p1.m_coeffs.head( n + 1 ) == p2.m_coeffs.head( n + 1 ) ).all();
}
bool operator!=( const Polynomial& p1, const Polynomial& p2 )
{
    return !( p1 == p2 );
}

int Polynomial::size() const
{
    return static_cast<int>( m_coeffs.size() );
}

int Polynomial::degree() const
{
    int n = this->size() - 1;
    while ( n > -1 && m_coeffs[n] == 0.0 )
        --n;
    return n;
}

CoeffType Polynomial::operator[]( int index ) const
{
    if( index < 0 ) throw std::out_of_range(
                "Negative index in Polynomial::operator[]" );
    return index < size() ? m_coeffs[index] : 0;
}

// Miscallaneous operations

double Polynomial::evaluate( double x ) const
{
    double val = 0.0;
    for ( int i = size(); i > 0; i-- ) {
        val = val * x + m_coeffs[i - 1];
        if ( i == 0 )
            break;
    }
    return val;
}

Polynomial Polynomial::integrate( double constant ) const
{
    int n = degree();
    Array1d coeffs = Array1d::Zero( n + 2 );
    coeffs[0] = constant;
    coeffs.tail( n + 1 ) = m_coeffs.head( n + 1 ) /
            Array1d::LinSpaced( n + 1, 1, n + 1 );
    return Polynomial( coeffs );
}
Polynomial Polynomial::differentiate() const
{
    int n = degree();
    if( n > -1 )
        return Polynomial( m_coeffs.segment( 1, n ) *
                Array1d::LinSpaced( n, 1, n ) );
    else
        return *this;
}

// Unary operators

Polynomial operator+( const Polynomial &p )
{
    return p;
}

Polynomial operator-( const Polynomial &p )
{
    return Polynomial( -p.m_coeffs );
}

// Arithmetic operations

void add_to( Array1d& s1, const Array1d& s2 )
{
    size_t len1 = s1.size();
    size_t len2 = s2.size();
    if ( len1 < len2 ) {
        Array1d s3( len2 );
        s3.head( len1 ) = s1 + s2.head( len1 );
        s3.tail( len2 - len1 ) = s2.tail( len2 - len1 );
        s3.swap( s1 );
    }
    else if ( len2 > 0 ) {
        s1.head( len2 ) += s2;
    }
}

void Polynomial::operator+=( const Polynomial &p2 )
{
    add_to( this->m_coeffs, p2.m_coeffs );
}

void Polynomial::operator+=( const CoeffType &c )
{
    if( this->size() > 0 )
        this->m_coeffs[0] += c;
    else
        this->m_coeffs.setConstant( 1, c );
}

Polynomial operator+( const Polynomial &p1, const Polynomial &p2 )
{
    Polynomial p = p1;
    add_to( p.m_coeffs, p2.m_coeffs );
    return p;
}

Polynomial operator+( const Polynomial &p, const CoeffType &c )
{
    Polynomial ret( p.m_coeffs );
    if ( !ret.size() ) {
        ret.m_coeffs.setConstant( 1, c );
    }
    else {
        ret.m_coeffs[0] += c;
    }
    return ret;
}

Polynomial operator+( const CoeffType &c, const Polynomial &p )
{
    return operator+( p, c );
}

void subtract( Array1d& s1, const Array1d& s2 )
{
    size_t len1 = s1.size();
    size_t len2 = s2.size();
    if ( len1 < len2 ) {
        Array1d s3( len2 );
        s3.head( len1 ) = s1 - s2.head( len1 );
        s3.tail( len2 - len1 ) = -s2.tail( len2 - len1 );
        s3.swap( s1 );
    }
    else if ( len2 > 0 ) {
        s1.head( len2 ) -= s2;
    }
}

void Polynomial::operator-=( const Polynomial &p2 )
{
    subtract( this->m_coeffs, p2.m_coeffs );
}

void Polynomial::operator-=( const CoeffType &c )
{
    this->operator+=( -c );
}

Polynomial operator-( const Polynomial &p1, const Polynomial &p2 )
{
    Polynomial p = p1;
    subtract( p.m_coeffs, p2.m_coeffs );
    return p;
}

Polynomial operator-( const Polynomial &p, const CoeffType &c )
{
    return operator+( p, -c );
}

Polynomial operator-( const CoeffType &c, const Polynomial &p )
{
    Polynomial q = operator+( p, -c );
    q.m_coeffs = -q.m_coeffs;
    return q;
}

void Polynomial::operator*=( const Polynomial &p2 )
{
    Polynomial r = ( *this ) * p2;
    this->m_coeffs = r.m_coeffs;
}

void Polynomial::operator*=( const CoeffType &c )
{
    this->m_coeffs *= c;
}

Polynomial operator*( const Polynomial &p1, const Polynomial &p2 )
{
    int n1 = p1.degree();
    int n2 = p2.degree();
    int n = n1 + n2 + 1;
    Polynomial ret;
    ret.m_coeffs.setZero( n + 1 );
    for ( int i1 = 0; i1 <= n1; i1++ ) {
        for ( int i2 = 0; i2 <= n2; i2++ ) {
            ret.m_coeffs[i1 + i2] += p1[i1] * p2[i2];
        }
    }
    return ret;
}

Polynomial operator*( const Polynomial &p, const CoeffType &c )
{
    return Polynomial( p.m_coeffs * c );
}

Polynomial operator*( const CoeffType &c, const Polynomial &p )
{
    return Polynomial( p.m_coeffs * c );
}

void Polynomial::operator/=( const CoeffType &c )
{
    this->m_coeffs /= c;
}

Polynomial operator/( const Polynomial &p, const CoeffType &c )
{
    return Polynomial( p.m_coeffs / c );
}

// Formatted IO

// Other functions

Polynomial::IOFormat Polynomial::default_format;

std::ostream &operator<<( std::ostream &os, const Polynomial &p )
{
    return p.put_formatted( os, Polynomial::default_format );
}

Polynomial::WithFormat Polynomial::format( const Polynomial::IOFormat &format )
const
{
    return Polynomial::WithFormat( format, this );
}

std::ostream &Polynomial::put_formatted( std::ostream &os,
        const Polynomial::IOFormat &format ) const
{
    bool first = true;
    int degree = this->degree();
    for ( int ind = 0; ind <= degree; ind++ ) {
        int i = format.m_highest_first ? degree - ind : ind;
        CoeffType coeff = m_coeffs[i];
        bool non_neg = coeff >= 0;
        CoeffType abs_coeff = non_neg ? coeff : -coeff;

        if( coeff == 0.0 )
            continue;

        if ( first )
            os << ( non_neg ? "" : "-" );
        else
            os << ( format.m_compact ? "" : " " ) <<
            ( non_neg ? "+"  : "-" ) << ( format.m_compact ? "" : " " );

        if ( i == 0 || abs_coeff != 1 )
            os << abs_coeff << ( i ? format.m_mult_symbol : "" );
        if ( i )
            os << format.m_symbol;
        if ( i > 1 )
            os << format.m_exp_symbol << i;
        first = false;
    }
    if( first )
        os << 0.0;

    return os;
}

std::ostream &operator<<( std::ostream &os, const Polynomial::WithFormat &pwf )
{
    return pwf.m_poly->put_formatted( os, pwf.m_format );
}

string Polynomial::to_string() const
{
    std::stringstream buffer;
    buffer << *this;
    return buffer.str();
}

Polynomial::WithFormat::WithFormat( Polynomial::IOFormat format,
        const Polynomial* poly )
    : m_format( format ), m_poly( poly ) {}

} // namespace Alea
