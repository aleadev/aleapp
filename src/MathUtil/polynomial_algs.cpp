/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file polynomial_algs.cpp
 * @brief Implementation file for Bases/polynomial_algs.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/MathUtil/polynomial_algs.hpp"
#include "Alea/src/MathUtil/polynomial.hpp"

#include <Eigen/Eigenvalues>

namespace Alea {

/**
 * @brief Evaluate polynomial system at multiple points.
 *
 * Evalute the polynomials system, specified by the recurrence
 * coefficients in rc at all the points given in xi. The returned array has
 * ... need to check the ordering of the dimensions, whether this is good...
 *
 * @param rc The recurrence coefficients as \f$p+1 \times 3\f$ array with
 *           columns a, b and c. Need p+1 rows for norms up to degree p.
 * @param xi The points at which to evaluate the polynomials.
 * @return alea::Array1d, containing evaluated polynomials at all the points
 */
ArrayXd poly_eval( const Array3d& rc, const Array1d& xi )
{
    long int p = rc.rows();
    long int k = xi.size();
    ArrayXd y( k, p + 1 );

    //ALEA_DISALLOW_EIGEN_MALLOC;
    Array1dCRef a = rc.col( 0 );
    Array1dCRef b = rc.col( 1 );
    Array1dCRef c = rc.col( 2 );

    y.col( 0 ) = 1;
    for( int n = 0; n < p; n++ ) {
        y.col( n + 1 ) = ( a( n ) + b( n ) * xi ) * y.col( n );
        if( n > 0 ) {
            y.col( n + 1 ) -= c( n ) * y.col( n - 1 );
        }
    }

    return y.transpose();
}

namespace internal {

template <typename ValueType>
struct poly_eval_impl {
    static vector<ValueType> evaluate( const Array3d& rc, const ValueType& xi,
                const ValueType& one )
    {
        typedef typename vector<ValueType>::size_type index_type;
        index_type p = static_cast<index_type>( rc.rows() );
        vector<ValueType> y( p + 1 );

        Array1dCRef a = rc.col( 0 );
        Array1dCRef b = rc.col( 1 );
        Array1dCRef c = rc.col( 2 );

        y[0] = one;
        for( index_type n = 0; n < p; n++ ) {
            y[n + 1] = ( a( n ) + b( n ) * xi ) * y[n];
            if( n > 0 ) {
                ValueType v = c( n ) * y[n - 1];
                y[n + 1] -= v;
            }
        }

        return y;
    }
};

} // namespace internal

vector<double> poly_eval( const Array3d& rc, const double& xi )
{
    return internal::poly_eval_impl<double>::evaluate( rc, xi, 1.0 );
}

vector<Polynomial> poly_eval( const Array3d& rc, const Polynomial& xi )
{
    return internal::poly_eval_impl<Polynomial>::evaluate( rc, xi,
            Polynomial::constant( 1 ) );
}

/**
 * @brief Compute the square norm of the polynomials via recurrence coefficients.
 *
 * This is based on the neat article:
 *
 *     Alan G. Law and M. B. Sledd "Normalizing Orthogonal Polynomials by
 *     Using their Recurrence Coefficients", Proceedings of the American
 *     Mathematical Society, Vol. 48, No. 2, (Apr., 1975), pp. 505 - 507
 *     URL: http://www.jstor.org/stable/2040291
 *
 * The formula for computing the square norm \f$h_n\f$ of polynomial \f$P_n\f$
 * given the recurrence \f$P_{n+1}(x)=(a_n+b_n x)P_n(x)-c_n P_{n-1}(x)\f$ is
 *
 * \f[ h_n = h_0 c_1 \cdot \cdots \cdot c_n b_n / b_0\f]
 *
 * @param rc The recurrence coefficients as \f$p+1 \times 3\f$ array with
 *           columns a, b and c. Need p+1 rows for norms up to degree p.
 * @param norm0 The square norm \f$h_0\f$ of \f$P_0\f$. Default is 1.
 * @return alea::Array1d, containing the square norms \f$h_0,\ldots,h_p\f$
 */

Array1d poly_square_norm( const Array3d& rc, double norm0 )
{
    long int p = rc.rows() - 1;
    Array1d h( p + 1 );

    ALEA_DISALLOW_EIGEN_MALLOC;
    Array1dCRef a = rc.col( 0 );
    Array1dCRef b = rc.col( 1 );
    Array1dCRef c = rc.col( 2 );

    double b0 = b( 0 );
    double c1n = norm0;
    h( 0 ) = norm0;
    for( int n = 1; n < p + 1; n++ ) {
        c1n *= n ? c( n ) : 1;
        h( n ) = b0 / b( n ) * c1n;
    }
    return h;
}

/**
 * @brief Compute the recurrence coefficients of the monic polynomials.
 *
 * Let \f$B_n\f$ be the coefficient of the highest order term of the
 * polynomial \f$P_n\f$ which are given by the recurrence
 * \f$P_{n+1}(x)=(a_n+b_n x)P_n(x)-c_n P_{n-1}(x)\f$, i.e.
 * \f$B_n=b_0\cdot\cdot\cdots b_{n-1}\f$.
 * Then for the monic polynomials \f$M_n\f$ it holds that
 * \f$B_{n+1} M_{n+1}(x)=(a_n+b_n x)B_n M_n(x)-c_n B_{n-1} M_{n-1}(x)\f$.
 * Dividing both sides by \f$B_{n+1}\f$ leads to the recurrence for the
 * normalised \f$M_n\f$
 *
 *   \f[M_{n+1}(x)=(a_n B_n/B_{n+1} +b_n B_n/B_{n+1} x) M_n(x)-c_n B_{n-1}/B_{n+1} M_{n-1}(x)\f]
 *
 * Therefore, the new recurrence coefficients for the \f$M_n\f$ are given by
 *
 *   \f[\tilde a_n = a_n B_n/B_{n+1},\quad \tilde b_n=b_n B_n/B_{n+1}, \quad \tilde c_n=c_n B_{n-1}/B_{n+1}\f]
 *
 *   \f[\tilde a_n = a_n / b_n,\quad \tilde b_n=1, \quad \tilde c_n=c_n / (b_{n-1} b_n)\f]
 *
 * Note, that apparently to compute p monic coefficients p+1 coefficients
 * are needed as input.
 *
 * @param rc The recurrence coefficients as \f$p+1 \times 3\f$ array with
 *           columns a, b and c. Need p+1 rows for normalised coefficients
 *           up to degree p.
 * @return alea::Array3d, recurrence coefficients of the normalised coefficients.
 */
Array3d to_monic_rc( const Array3d& rc )
{
    long int p = rc.rows() - 1;
    Array3d monic_rc( p, 3 );

    ALEA_DISALLOW_EIGEN_MALLOC;
    Array1dCRef a = rc.col( 0 );
    Array1dCRef b = rc.col( 1 );
    Array1dCRef c = rc.col( 2 );

    monic_rc.col( 0 ).head( p ) = a.head( p ) / b.head( p );
    monic_rc.col( 1 ).head( p ) = 1;
    monic_rc.col( 2 ) ( 0 ) = 0;
    monic_rc.col( 2 ).segment( 1, p - 1 ) = c.segment( 1, p - 1 ) /
            ( b.head( p - 1 ) * b.segment( 1, p - 1 ) );

    return monic_rc;
}

/**
 * @brief Compute the recurrence coefficients of the normalised polynomials.
 *
 * Let \f$z_n\f$ be the norm of the polynomial \f$P_n\f$ which are given
 * by the recurrence \f$P_{n+1}(x)=(a_n+b_n x)P_n(x)-c_n P_{n-1}(x)\f$.
 * Then for the unnormalised polynomials \f$Q_n\f$ it holds that
 * \f$z_{n+1} Q_{n+1}(x)=(a_n+b_n x)z_n Q_n(x)-c_n z_{n-1} Q_{n-1}(x)\f$.
 * Dividing both sides by \f$z_{n+1}\f$ leads to the recurrence for the
 * normalised \f$Q_n\f$
 *
 *   \f[Q_{n+1}(x)=(a_n z_n/z_{n+1} +b_n z_n/z_{n+1} x) Q_n(x)-c_n z_{n-1}/z_{n+1} Q_{n-1}(x)\f]
 *
 * Therefore, the new recurrence coefficients for the \f$Q_n\f$ are given by
 *
 *   \f[\tilde a_n = a_n z_n/z_{n+1},\quad \tilde b_n=b_n z_n/z_{n+1}, \quad \tilde c_n=c_n z_{n-1}/z_{n+1}\f]
 *
 * Note, that apparently to compute p normalised coefficients p+1 coefficients
 * are needed as input.
 *
 * @param rc The recurrence coefficients as \f$p+1 \times 3\f$ array with
 *           columns a, b and c. Need p+1 rows for normalised coefficients
 *           up to degree p.
 * @return alea::Array3d, recurrence coefficients of the normalised coefficients.
 */

Array3d to_normalised_rc( const Array3d& rc )
{
    long int p = rc.rows() - 1;
    Array3d norm_rc( p, 3 );
    Array1d z = poly_square_norm( rc ).sqrt();

    ALEA_DISALLOW_EIGEN_MALLOC;
    Array1dCRef a = rc.col( 0 );
    Array1dCRef b = rc.col( 1 );
    Array1dCRef c = rc.col( 2 );

    norm_rc.col( 0 ).head( p ) = a.head( p ) * z.head( p ) / z.segment( 1, p );
    norm_rc.col( 1 ).head( p ) = b.head( p ) * z.head( p ) / z.segment( 1, p );
    norm_rc.col( 2 ) ( 0 ) = 0;
    norm_rc.col( 2 ).segment( 1, p - 1 ) = c.segment( 1, p - 1 )
            * z.head( p - 1 ) / z.segment( 2, p - 1 );

    return norm_rc;
}

/**
 * @brief Compute nodes and weights of a Gauss rule from recurrence
 *        coefficients.
 *
 * Implementation based on the article
 *
 *     W. Gautschi: "Orthogonal Polynomials and Quadrature", Electronic
 *     Transactions on Numerical Analysis. Volume 9, 1999, pp. 65-76.
 *
 * See also: http://dlmf.nist.gov/3.5.vi
 *
 * @param rc The recurrence coefficients as \f$p+1 \times 3\f$ array with
 *           columns a, b and c. Need p+1 rows for normalised coefficients
 *           up to degree p.
 * @return Quad1d, nodes and weights of the Gauss rule.
 */
Quad1d gauss_rule_from_rc( const Array3d& rc )
{
    long int p = rc.rows() - 1;
    Array3d monic_rc = to_monic_rc( rc );
    Matrix Jacobi = Matrix::Zero( p, p );

    Array1dCRef a = monic_rc.col( 0 );
    Array1dCRef c = monic_rc.col( 2 );

    for( int n = 0; n < p; n++ ) {
        Jacobi( n, n ) = -a( n );
        if( n ) {
            Jacobi( n, n - 1 ) = sqrt( c( n ) );
            Jacobi( n - 1, n ) = sqrt( c( n ) );
        }
    }

    Eigen::SelfAdjointEigenSolver<Matrix> ev_solver( Jacobi );

    Array1d x = ev_solver.eigenvalues();
    Array1d w = ev_solver.eigenvectors().row( 0 );
    w = w * w;
    w = w / w.sum();

    Quad1d q = {x, w};
    return q;
}

} // namespace Alea
