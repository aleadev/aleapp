Libraries
=========

catch for unit testing (much nicer than boost or google or cppunit)

Eigen for linear algebra

Do not use old C std library, because it's too non-portable

Build system
============

CMake

Notation and Naming
===================

C++ files end in cpp and hpp

Headers for user code are in Camel case without .hpp
e.g. "PolynomialSystem". All dependcies to other headers are
satisfied, recursive use of headers is ok. Headers for internal use
are with .hpp, no recursive includes allowed.

Classes are in Camel case. Methods are lowercase with
underscores. Member vars are prefixed by "m_".

Prerequisites
=============
Git, Mercurial, CMake, CXX compiler

Things to come
==============
SWIG for Python
demos in CXX and Python
API doc generation with doxygen

Roadmap
=======
Distributions
GPC germs
GPC bases
GPC
1D Integration rules
ND Tensor and Smolyak integration

Hierarchy of Header files (for the picky and for the lazy)





